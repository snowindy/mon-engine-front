package com.github.snowindy.mon.entity

import groovy.transform.ToString;

@ToString
public class HouseTopic {
    House house
    String title
    String url
    boolean active
    String uuid
    
    static mapping = {
        table 'HOUSE_TOPICS'
        version false
        
        house column:'HOUSE', lazy: false
    }
}
