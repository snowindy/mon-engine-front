package com.github.snowindy.mon.entity

import com.github.snowindy.util.JsonUtils;

import groovy.transform.ToString

@ToString
class MonItemFront {
    String title
    String url
    Date dateDetected
    String uuid
    String metadataString
    String textBody
    
    HouseTopic houseTopic
    
    Map getMetadata(){
        metadataString? JsonUtils.fromJsonToMapOfString(metadataString): [:]
    }

    static mapping = {
        table 'MON_ITEMS'
        version false
        
        houseTopic column:'HOUSE_TOPIC', lazy: false
        metadataString column:'ATTACHMENTS_TEXT', lazy: false, type: 'text'
        textBody column:'TEXT_BODY', lazy: false, type: 'text'
        dateDetected column:'DATE_DETECTED'
    }
}
