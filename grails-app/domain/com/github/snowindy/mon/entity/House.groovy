package com.github.snowindy.mon.entity

import groovy.transform.ToString;

@ToString
public class House {
    String domainName
    String urlPrefix
    boolean active
    String uuid
    
    static mapping = {
        table 'HOUSES'
        version false
        
        domainName column:'DOMAIN_NAME'
        urlPrefix column:'URL_PREFIX'
    }
}
