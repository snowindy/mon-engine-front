package com.github.snowindy.mon.entity

import groovy.transform.ToString

import org.codehaus.jackson.map.ObjectMapper

import com.github.snowindy.mon.entity.enums.AggregationPolicy
import com.github.snowindy.mon.matcher.MatcherSerializer;

@ToString(excludes='password')
class User {
    String name
    String password
    String query
    
    static hasMany = [housesOfInterest: House]
    static transients = [ "_instanceValidation"]

    static FIELDS_ALLOWED_TO_UPDATE = ['housesOfInterest', 'query', 
        'password', 'active', 'noSearchTill', 'aggregationPolicy']
    
    Date addedDate
    Date updatedDate
    Date noSearchTill
    Date paidTill
    boolean active = true
    String email
    String uuid = UUID.randomUUID().toString()
    
    AggregationPolicy aggregationPolicy = AggregationPolicy.MIXED

    List<String> getSelectedDomains(){
        housesOfInterest.collect{it.domainName}
    }

    void setSelectedDomains(List<String> selectedDomains){
        housesOfInterest = House.getAll().findAll{
            selectedDomains?.contains(it.domainName)
        }
    }
    
    def beforeInsert(){
        addedDate = new Date()
    }
    
    /**
     * Not beforeUpdate because we want to track updates even if actual user record is not updated.
     */
    def beforeValidate(){
        updatedDate = new Date()
    }

    //Only for the sake of whole instance validation
    String _instanceValidation

    static constraints = {
        name size: 2..250, blank: false, nullable: true
        email email: true, blank: false
        password size: 5..250, blank: false
        uuid blank: false, nullable: false
        aggregationPolicy nullable: false
        noSearchTill nullable: true
        addedDate nullable: true
        updatedDate nullable: true
        paidTill nullable: true
        query(validator: {
            if (!it){
                return "error.query.message"
            }
            MatcherSerializer matchSerializer = new MatcherSerializer()
            try{
                matchSerializer.deserializeJson(it)
            }catch(e){
                return "error.query.message"
            }
            true
        })
        housesOfInterest(validator: {
            if (!it){
                return "error.user.domains.noSelected"
            }
            true
        })
        _instanceValidation(nullable:true, validator: { val, instance, errors ->
            
        })
    }

    static mapping = {
        table 'USERS'
        version false

        noSearchTill column:'NO_SEARCH_TILL'
        addedDate column:'ADDED_DATE'
        updatedDate column:'UPDATED_DATE'
        
        aggregationPolicy column:'NTF_AGGREGATION'
        paidTill column:'PAID_TILL'
        query column:'query', type: 'text'

        housesOfInterest joinTable: [name: 'USERS_HOUSES_OF_INTEREST',
            key: 'users',
            column: 'HOUSES_OF_INTEREST'
        ], lazy: false
    }
}
