package com.github.snowindy.mon.entity

import groovy.transform.ToString;

@ToString
public class Token {
    String entityId
    EntityType entityType
    Date validUntil
    Date dateCreated
    Date dateUpdated
    String uuid
    
    def beforeInsert(){
        dateCreated = new Date()
    }
    
    /**
     * Not beforeUpdate because we want to track updates even if actual user record is not updated.
     */
    def beforeValidate(){
        dateUpdated = new Date()
    }
    
    static constraints = {
        entityId blank: false
        entityType nullable: false
        uuid blank: false
        dateCreated nullable: true
        dateUpdated nullable: true
        validUntil nullable: true
    }

    static mapping = {
        table 'TOKENS'
        version false

        entityId column:'ENTITY_ID'
        entityType column:'ENTITY_TYPE'
        validUntil column:'VALID_UNTIL'
        dateCreated column:'DATE_CREATED'
        dateUpdated column:'DATE_UPDATED'
    }
}
