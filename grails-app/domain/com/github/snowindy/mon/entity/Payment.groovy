package com.github.snowindy.mon.entity

import groovy.transform.ToString;

@ToString
public class Payment {
    String userUuid
    String params
    BigDecimal amount
    Date dateCreated
    
    def beforeInsert(){
        dateCreated = new Date()
    }

    static constraints = {
        userUuid blank: false
        params blank: false
        amount nullable: false
        dateCreated nullable: true
    }

    static mapping = {
        table 'MON_PAYMENTS'
        version false

        userUuid column:'user_uuid'
        params column:'params', type: 'text'
        amount column:'amount'
        dateCreated column:'DATE_CREATED'
    }
}
