package com.github.snowindy.mon.jobs

import org.apache.commons.lang3.StringUtils
import org.codehaus.groovy.grails.commons.ApplicationHolder
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.cache.CacheManager

import com.github.snowindy.mon.Constants


class CacheEvictJob {
    
    private static REST_CACHE_TRIGGER_NAME = 'restCleanupTrigger'
    private static REST_SNAPSHOT_CACHE_TRIGGER_NAME = 'restSnapshotCleanupTrigger'
    private static REST_STATS_CACHE_TRIGGER_NAME = 'restStatsCleanupTrigger'
    
    static triggers = {
        simple name: REST_CACHE_TRIGGER_NAME, 
            startDelay: ApplicationHolder.application.config.cache.rest.evict.startDelaySecons*1000, 
            repeatInterval: ApplicationHolder.application.config.cache.rest.evict.repeatIntervalSecons*1000
            
        simple name: REST_SNAPSHOT_CACHE_TRIGGER_NAME, 
            startDelay: ApplicationHolder.application.config.cache.rest.snapshot.evict.startDelaySecons*1000, 
            repeatInterval: ApplicationHolder.application.config.cache.rest.snapshot.evict.repeatIntervalSecons*1000
            
        simple name: REST_STATS_CACHE_TRIGGER_NAME,
            startDelay: ApplicationHolder.application.config.cache.rest.stats.evict.startDelaySecons*1000,
            repeatInterval: ApplicationHolder.application.config.cache.rest.stats.evict.repeatIntervalSecons*1000
    }
    def group = "CacheEvictGroup"

    CacheManager grailsCacheManager

    private final static Logger logger = LoggerFactory.getLogger(CacheEvictJob.class);

    static boolean noRefresh = StringUtils.isNotBlank(System.getProperty("cache.norefresh"))
    
    def execute(context){
        if (!noRefresh){
            if (REST_CACHE_TRIGGER_NAME == context.trigger.name){
                evictCacheFully(Constants.Cache.REST)
            }
            if (REST_SNAPSHOT_CACHE_TRIGGER_NAME == context.trigger.name){
                evictCacheFully(Constants.Cache.REST_SNAPSHOT)
            }
            if (REST_STATS_CACHE_TRIGGER_NAME == context.trigger.name){
                evictCacheFully(Constants.Cache.REST_STATS)
            }
        }
    }
    
    
    private def evictCacheFully(name){
        logger.debug("Evicting $name cache")
        def restCache = grailsCacheManager?.getCache(name)
        if (restCache){
            if (restCache.metaClass.respondsTo(restCache, "getAllKeys")){
                def allKeys = restCache.getAllKeys()
                allKeys.each{ restCache.evict(it) }
            }else{
                throw new Exception("Method getAllKeys() not found for cache $restCache")
            }
        }
    }
}
