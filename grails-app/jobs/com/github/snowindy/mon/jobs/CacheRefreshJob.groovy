package com.github.snowindy.mon.jobs

import org.apache.commons.lang3.StringUtils
import org.codehaus.groovy.grails.commons.ApplicationHolder
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.cache.CacheManager

import com.github.snowindy.mon.entity.House
import com.github.snowindy.mon.front.ViewSupportService


class CacheRefreshJob {
    
    private static REST_CACHE_REFRESH_TRIGGER_NAME = 'restRefreshTrigger'
    
    static triggers = {
        simple name: REST_CACHE_REFRESH_TRIGGER_NAME, 
            startDelay: ApplicationHolder.application.config.cache.rest.refresh.startDelaySecons*1000, 
            repeatInterval: ApplicationHolder.application.config.cache.rest.refresh.repeatIntervalSecons*1000
       
    }
    def group = "CacheRefreshGroup"

    CacheManager grailsCacheManager
    
    ViewSupportService viewSupportService

    private final static Logger logger = LoggerFactory.getLogger(CacheRefreshJob.class);

    static boolean noRefresh = StringUtils.isNotBlank(System.getProperty("cache.norefresh"))
    
    def execute(context){
        if (!noRefresh){
            logger.trace("Refreshing cache values.")
            viewSupportService.getRecentMonItems()
            
            viewSupportService.getHouses().each { House h ->
                viewSupportService.getHouseItemCountForPeriod(h)
            }
        }
    }
}
