<div class="control-group ${invalid ? 'error' : ''}">
	<div class="controls">
		<%= widget %>
	</div>
</div>