<g:if test="${!inputClass}">
	<g:set var="inputClass" value="span4" />
</g:if>
<g:set var="decorationClass" value="" />
<recaptcha:ifFailed>
	<g:set var="decorationClass" value="error" />
</recaptcha:ifFailed>
<div id="recaptcha_widget" style="display: none">
	<div class="control-group ${decorationClass}">
		<label class="control-label"><g:message code="home.form.captcha.label" /></label>
		<div class="controls">
			<div id="recaptcha_image" title="${message(code:'home.captcha.img.title')}" onclick="javascript:Recaptcha.reload()"
				style="width: 300px; height: 57px;"></div>
			<input placeholder="${message(code:'home.captcha.input.placeholder')}" class="${inputClass}" id="recaptcha_response_field" name="recaptcha_response_field" type="text" autocomplete="off" />
			<recaptcha:ifFailed>
				<span class="help-inline"><g:message code="error.incorrectCaptcha.message" /></span>
			</recaptcha:ifFailed>
		</div>
	</div>
</div>
