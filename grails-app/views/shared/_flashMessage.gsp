<%@page defaultCodec="none" %>

<div class="app-alert">
	<g:if test="${flash.message}">
		<bootstrap:alert class="alert-info">
			${flash.message}
		</bootstrap:alert>
	</g:if>
	<g:if test="${params.messages}">
		<g:each in="${params.messages}" var="msg">
			<bootstrap:alert class="alert-info">
				${msg}
			</bootstrap:alert>
		</g:each>
	</g:if>
	<g:if test="${flash.messages}">
		<g:each in="${flash.messages}" var="msg">
			<bootstrap:alert class="alert-info">
				${msg}
			</bootstrap:alert>
		</g:each>
	</g:if>
</div>
<div class="app-warning">
	<g:if test="${flash.warning}">
		<bootstrap:alert class="warning-info">
			${flash.warning}
		</bootstrap:alert>
	</g:if>
	<g:if test="${params.warning}">
		<bootstrap:alert class="warning-info">
			${params.warning}
		</bootstrap:alert>
	</g:if>
	<g:if test="${params.warnings}">
		<g:each in="${params.warnings}" var="msg">
			<bootstrap:alert class="warning-info">
				${msg}
			</bootstrap:alert>
		</g:each>
	</g:if>
	<g:if test="${flash.warnings}">
		<g:each in="${flash.warnings}" var="msg">
			<bootstrap:alert class="warning-info">
				${msg}
			</bootstrap:alert>
		</g:each>
	</g:if>
</div>