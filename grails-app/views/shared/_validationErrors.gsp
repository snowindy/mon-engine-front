<g:if test="${!com.github.snowindy.mon.front.CommonService.isValid(domains) || params.validationErrors}">
	<div class="app-validation">
		<g:each in="${domains}" var="domain">
			<g:hasErrors bean="${domain}">
				<bootstrap:alert class="alert-error">
					<ul>
						<g:eachError bean="${domain}" var="error">
							<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
									error="${error}" /></li>
						</g:eachError>
					</ul>
				</bootstrap:alert>
			</g:hasErrors>
		</g:each>

		<g:if test="${params.validationErrors}">
			<bootstrap:alert class="alert-error">
				<ul>
					<g:each in="${params.validationErrors}" var="error">
						<li>
							${error}
						</li>
					</g:each>
				</ul>
			</bootstrap:alert>
		</g:if>
	</div>
</g:if>