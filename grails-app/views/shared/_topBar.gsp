<g:if test="${!loginCmd}">
	<g:set var="loginCmd" value="${new com.github.snowindy.mon.front.cmd.LoginCommand()}"></g:set>
</g:if>

<div id="topbar-wrap">
	<div class="container" id="topbar">
		<div class="span4 topbar-link">
			<g:link controller="root" params="[lang:'en']">EN</g:link>
			|
			<g:link controller="root" params="[lang:'ru']">RU</g:link>
		</div>

		<g:if test="${session.loggedIn}">
			<g:form class="navbar-form pull-right logout-form" controller="root" action="signOut">
				<span id="logged-in-user"><g:message code="home.loggedIn.as.message" args="[session.user.email]" /></span>
				<g:submitButton class="btn btn-inverse span2" name="submit" value="${message(code:'home.logout.submit.title')}" />
			</g:form>
		</g:if>
		<g:else>
			<div class="span3 topbar-link reset-password">
				<g:link class="topbar-link" controller="passwordReset">
					<g:message code="home.login.resetPassword" />
				</g:link>
			</div>
			<g:form class="navbar-form pull-right login-form" controller="root" action="signIn">
				<f:input input-class="span2" bean="loginCmd" property="login"
					input-placeholder="${message(code:'home.login.email.placeholder')}" />
				<f:input input-class="span2" bean="loginCmd" property="password" input-type="password"
					input-placeholder="${message(code:'home.login.password.placeholder')}" />

				<g:hiddenField name="redirectAfterLogin" value="${redirectAfterLogin}" />

				<g:submitButton class="btn btn-inverse span2" name="submit" value="${message(code:'home.login.submit.title')}" />

			</g:form>
		</g:else>
	</div>
</div>