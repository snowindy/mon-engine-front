<header>
	<g:render template="/shared/topBar" />
	<div class="container">
		<h1>
			<g:link controller="root">
				<g:message code="home.welcome.header" />
			</g:link>
		</h1>
		<p class="lead">
			<g:message code="home.welcome.message.p1" />
		</p>
	</div>
</header>