<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap" />
<title><g:message code="unsubscribe.title" /></title>
</head>

<body>
 	<g:render template="/shared/headerSecondaryPage" />

	<div class="container">
		<g:render template="/shared/flashMessage" />
		<g:render template="/shared/validationErrors" model="[domains:[unsubscribeCmd]]" />

		<h3>
			<g:message code="unsubscribe.form.header" />
		</h3>
		<g:form>
			<g:hiddenField name="user" value="${params.user}"/>
			<div class="row">
				<div class="span6">
					<p>
						<g:message code="unsubscribe.form.warning.message" />
					</p>
					<p>
						<g:link controller="root">
							<g:message code="common.to.home.page" />
						</g:link>
					</p>

				</div>
				<div class="span4">
					<f:field input-class="span4" bean="unsubscribeCmd" property="timeoutHours" />

					<g:actionSubmit class="btn btn-large btn-block btn-primary" action="unsubscribe"
						value="${message(code:"unsubscribe.form.unsubscribe.label")}" />

				</div>
			</div>
		</g:form>


	</div>
</body>
</html>
