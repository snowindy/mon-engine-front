<div class="control-group ${invalid ? 'error' : ''}">
	<label class="control-label" for="${property}">${label}${required?' *':''}</label>
	<div class="controls">
		<%= widget %>
		<g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
	</div>
</div>