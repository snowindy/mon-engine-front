<g:each in="${houses}" var="house">
	<label class="checkbox"> <g:checkBox name="${prefix}${property}" value="${house.domainName}"
			checked="${value?.contains(house.domainName)}" /> ${house.domainName}
	</label>
</g:each>