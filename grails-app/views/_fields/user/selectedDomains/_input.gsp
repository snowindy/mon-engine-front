<table class="table dashed-vertical selected-houses">
	<tr>
		<th><g:message code="user.houses.global" /></th>
		<td><g:render template="/_fields/user/selectedDomains/houses" model="[houses:globalHouses]" /></td>

	</tr>
	<tr>
		<th><g:message code="user.houses.local" /></th>
		<td><g:render template="/_fields/user/selectedDomains/houses" model="[houses:localHouses]" /></td>
	</tr>
</table>
