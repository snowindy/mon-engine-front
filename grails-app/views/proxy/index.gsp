<%@page defaultCodec="none"%>
<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap" />
<title><g:message code="home.title.proxyWait" /></title>
</head>

<body>
	<header>
		<g:render template="/shared/headerSecondaryPage" />
	</header>

	<div class="container js-proxy-page">
		<g:render template="/shared/flashMessage" />

		<div class="row padded">
			<div class="span6 col-centered" id="countdown">
				<h4>
					<g:set var="p1">
					<span id=seconds></span></g:set>
					<g:message code="proxy.transferring.in.seconds" encodeAs="none" args="[p1]"/>
				</h4>
				<p>
					<span> <em>${proxyServiceResult.monItem.title.encodeAsHTML()}</em>
					</span> (<a href="http://${proxyServiceResult.monItem.houseTopic.house.domainName}">${proxyServiceResult.monItem.houseTopic.house.domainName}</a>)
				</p>
			</div>
			<div class="span6 col-centered" id="aftercount" style="display: none;">
				<h4><g:message code="proxy.redirecting.to.target" />
				(<a  href="${nextHopUrl}"><g:message code="common.link" /></a>)</h4>
			</div>
			
		</div>
		<hr/>
		<div class="row padded">
			<div class="span6 col-centered">
				<h4><g:message code="proxy.payment.teaser.title" /></h4>
				<table class="table dashed-vertical">
					<tbody>
						<tr>
							<th>
								<a id="payButton" href="${createLink(controller:'payment', params:[user:proxyServiceResult.user.uuid])}" 
					class="btn btn-success" role="button" target="_blank"><g:message code="proxy.payment.button.label" /></a>
								<div style="text-align: center;">
									<g:message code="proxy.payment.cmon.label" />
								</div>
							</th>
							<td><g:message code="proxy.payment.teaser.text" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>


		<script type="text/javascript">
			var lm = lm||{};
			lm.proxy = {};
			lm.proxy.nextHopUrl = '${nextHopUrl}';
			lm.proxy.waitSeconds = ${proxyServiceResult.waitSeconds};
			lm.proxy.itemTitle = '${proxyServiceResult.monItem.title.encodeAsHTML()}';
			lm.proxy.itemHouse = '${proxyServiceResult.monItem.houseTopic.house.domainName}';
			lm.proxy.forceWatchPage = ${forceWatchPage};

			$(function() {
				lm.initWaitScreen();
			});
		</script>
	</div>
</body>
</html>
