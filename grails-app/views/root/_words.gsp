<g:if test="${!inputClass}">
	<g:set var="inputClass" value="span4" />
</g:if>

<script>
	var lm = lm || {};
	lm.query = <g:message code="raw.output" args="[user.query?:'null']" encodeAs="none"/>;
</script>

<input type="hidden" name="query" id="formUserQueryInput" />
