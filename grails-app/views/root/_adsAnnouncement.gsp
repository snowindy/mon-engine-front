<g:if test="${grailsApplication.config.topAnnouncement.ads}">
	<g:render template="/shared/topAnnouncement" model="[text: grailsApplication.config.topAnnouncement.ads, cssClass:'super-top-ads']"/>
</g:if>