<hr />
<div id="recent-items-row" class="row">
	<div id="recent-items" class="span8">
		<g:render template="recentItems" />
	</div>

	<div id="stats" class="span4">
		<g:render template="countStats" />
	</div>
</div>