<g:if test="${grailsApplication.config.topAnnouncement.goodNews}">
	<g:render template="/shared/topAnnouncement" model="[text: grailsApplication.config.topAnnouncement.goodNews, cssClass:'super-top-good-news']"/>
</g:if>