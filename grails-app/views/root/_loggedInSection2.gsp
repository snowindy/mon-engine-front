<div class="span4">
	<h3>
		<g:message code="home.settings.header" />
	</h3>
	<p class="step-hint">
		<g:message code="home.step.keywords.hint" />
	</p>
	<f:field bean="user" input-valueMessagePrefix="aggregationPolicy"
		property="aggregationPolicy" />
</div>

<div class="span4">
	<h3>
		<g:message code="home.actions.header" />
	</h3>
	<g:actionSubmit onClick="return lm.profileUpdate.onSubmit()" id="main-submit"
		class="btn btn-large btn-block btn-primary"
		action="processNotificationRequest"
		value="${message(code:"home.form.update.label")}" />
	<g:link controller="unsubscribe"
		params="[user:session?.user?.uuid, timeoutHours:24]"
		class="btn btn-large btn-block btn-info">
		<g:message code="home.form.suspend.label" />
	</g:link>
	<hr />
	<g:if test="${session.user.active}">
		<g:actionSubmit class="btn btn-large btn-block btn-warning"
			action="deactivateSubscription"
			value="${message(code:'home.form.deactivate.label')}" />
	</g:if>
	<g:else>
		<g:actionSubmit class="btn btn-large btn-block btn-success"
			action="activateSubscription"
			value="${message(code:'home.form.activate.label')}" />
	</g:else>

	<g:render template="wordsHint" />
</div>