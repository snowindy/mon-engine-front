<p id="wordform-hint" style="display: none;">
	<span class="label label-info"><g:message
			code="home.keywords.wordform.hint.label" /></span>
	<g:message code="home.keywords.wordform.hint" />
	<br /> <span class="label label-info"><g:message
			code="home.keywords.wordcase.hint.label" /></span>
	<g:message code="home.keywords.wordcase.hint" />
</p>