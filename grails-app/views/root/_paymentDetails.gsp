<g:if test="${showPaymentDetails}">
	<div class="row">
		<g:if test="${session.user.paidTill > new Date()}">
			<div class="span4"><g:message code="home.payment.paid.till" /> ${session.user.paidTill.format('yyyy-MM-dd')}</div>
		</g:if>
		<div class="span4">
			<g:link controller="payment" params="[user:session.user.uuid]">
					<g:message code="home.payment.link.label" />
				</g:link>
		</div>
	</div>
</g:if>