<g:if test="${grailsApplication.config.topAnnouncement.warning}">
	<g:render template="/shared/topAnnouncement" model="[text: grailsApplication.config.topAnnouncement.warning, cssClass:'super-top-warning']"/>
</g:if>