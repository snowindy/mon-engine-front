<div class="span2">
</div>
<div class="span6">
	<h3>
		2. <g:message code="home.steps.identity.header" />
	</h3>
	<f:field input-class="span4" bean="user" property="email"
		input-placeholder="${message(code:'home.form.email.placeholder')}" />
	
	<f:field input-class="span4" bean="user" property="password" input-type="password"
		input-placeholder="${message(code:'home.form.password.placeholder')}" />
	
	<g:if test="${!session.captchaGuessOk}">
		<recaptcha:ifEnabled>
			<g:render template="/shared/captcha/simpleCaptcha" model="[inputClass:'span4']" />
			<recaptcha:recaptcha theme="custom" lang="en" custom_theme_widget="recaptcha_widget" />
		</recaptcha:ifEnabled>
	</g:if>
	
	<g:render template="wordsHint" />
</div>