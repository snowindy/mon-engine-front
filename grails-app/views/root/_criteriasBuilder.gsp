<div class="row" id="criteriasBuilderHolder" style="padding-top: 10px; padding-bottom: 40px;">
	<div class="span7">
		<g:render template="criteriaSample" />
		<div id="criteriasBuilder" ></div>
		<p id="emptyCriteriaHint" class="large-hint">
			<g:message code="criteria.builder.empty.hint" />
		</p>
		
		<p class="large-hint">
			<g:message code="criteria.builder.knowHow" />
		</p>
	</div>
	<div class="span4 dashed-separated">
		<h4 style="margin-top: 0;">
			<g:message code="criteria.builder.filters" />
		</h4>
		<ul class="unstyled">
			<li>
				<a href="javascript:lm.criteria.addTitle();void(0);">+ <g:message code="criteria.builder.samples.title" /></a>
			</li>
			<li>
				<a href="javascript:lm.criteria.addSkills();void(0);">+ <g:message code="criteria.builder.samples.skills" /></a>
			</li>
			<li>
				<a href="javascript:lm.criteria.addCategory();void(0);">+ <g:message code="criteria.builder.samples.category" /></a>
			</li>
			<li>
				<a href="javascript:lm.criteria.addSimplePhrases();void(0);">+ <g:message code="criteria.builder.samples.simple" /></a>
			</li>
			<li>
				<a href="javascript:lm.criteria.addSimplePhrasesWithStop();void(0);">+ <g:message code="criteria.builder.samples.simpleAndStop" /></a>
			</li>
			<li>
				<a href="javascript:lm.criteria.addBudgetGTE();void(0);">+ <g:message code="criteria.builder.samples.budgetGTE" /></a>
			</li>
			<li>
				<a href="javascript:lm.criteria.addBudgetUnknownOrGTE();void(0);">+ <g:message code="criteria.builder.samples.budgetUnknownOrGTE" /></a>
			</li>
			<li>
				<a href="javascript:lm.criteria.addHourlyRateUnknownOrGTE();void(0);">+ <g:message code="criteria.builder.samples.rateUnknownOrGTE" /></a>
			</li>
			<li>
				<a href="javascript:lm.criteria.addClientCountry();void(0);">+ <g:message code="criteria.builder.samples.clientCountry" /></a>
			</li>
		</ul>
	</div>
</div>