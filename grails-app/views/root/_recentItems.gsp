
<h3>
	<g:message code="home.recent.items.header" /><a id="items-recent">&nbsp;</a>
</h3>
<g:each in="${recentMonItems}" var="item">
	<div class="recent-item">
		<div class="item-body">
			<g:link controller="monItem" action="index" class="recent-item-icon" title="${item.title}" target="_blank" params="[itemUuid: item.uuid]">
				${item.title}
			</g:link>
		</div>
		<div class="item-meta">
			<div class="item-meta-section1">
				<a href="${item.url}" class="recent-item-icon" title="${item.houseTopic.house.domainName}"> 
				<img src="${resource(dir:'images/mp-icons', file: "${item.houseTopic.house.domainName}.ico")}" />
				<span> ${item.houseTopic.house.domainName} </span>
				</a>
			</div>
			<div class="item-meta-section2">${recentMonItemTimes[item]}</div>
		</div>
	</div>
</g:each>