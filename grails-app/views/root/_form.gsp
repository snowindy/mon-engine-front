<g:form class="subscribe-form" controller="root">
	<div class="row">
		<div class="span4">
			<div id="marketplaces">
				<h3>
					${session.loggedIn? '': '1.'} <g:message code="home.steps.marketplaces.header" />
				</h3>
				<p class="step-hint">
					<g:message code="home.step.marketplaces.hint" />
				</p>
				<f:field bean="user" property="selectedDomains" />
			</div>
		</div>
		<g:if test="${session.loggedIn}">
			<g:render template="loggedInSection2" />
		</g:if>
		<g:else>
			<g:hiddenField name="aggregationPolicy" value="${user.aggregationPolicy}"/>
			<g:render template="notLoggedInSection2" />
		</g:else>
	</div>
	<div class="row">
		<div class="span12">
			<h3>
				${session.loggedIn? '': '3.'} <g:message code="home.steps.criterias.header" />
			</h3>
			<g:render template="words" />
		</div>
	</div>
	<g:render template="criteriasBuilder" />
	<g:if test="${!session.loggedIn}">
		<div class="row">
			<div class="span2">
	
			</div>
			<div class="span8">
				<g:actionSubmit onClick="return lm.profileUpdate.onSubmit()" id="main-submit" class="btn btn-large btn-block btn-primary top-offset" action="processNotificationRequest"
					value="${message(code:"home.form.signup.label")}" />
			</div>
		</div>
	</g:if>
</g:form>