<h3>
	<g:message code="home.stats.count.header" />
</h3>
<table class="table table-striped">
	<g:each in="${countStats}" var="item">
		<tr>
			<td><img src="${resource(dir:'images/mp-icons', file: "${item.key}.ico")}" /> <a href="http://${item.key}">
					${item.key}
			</a></td>

			<td><g:message code="home.stats.job.message" args="[item.value]" /></td>
		</tr>
	</g:each>
</table>
