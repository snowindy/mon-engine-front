<div id="criteriaSampleContainer" style="display: none;">
	<div class="criteria js-criteria">
		<div class="row-fluid">
				<div class="span8 text-left"><strong class="js-criteria-title">
				<span class="negation js-criteria-negation" style="display: none;"><g:message code="criteria.negation"/></span>
<span class="js-criteria-title-containsAnyPhrase" style="display:none;"><g:message code="criteria.containsAnyPhrase"/></span>
<span class="js-criteria-title-containsAllPhrase" style="display:none;"><g:message code="criteria.containsAllPhrase"/></span>
<span class="js-criteria-title-lineStartsWithSubstring" style="display:none;"><g:message code="criteria.lineStartsWithSubstring"/></span>
<span class="js-criteria-title-lineStartsWithSubstringContainsAnyPhrase" style="display:none;"><g:message code="criteria.lineStartsWithSubstringContainsAnyPhrase"/></span>
<span class="js-criteria-title-lineStartsWithSubstringFirstNumberGTE" style="display:none;"><g:message code="criteria.lineStartsWithSubstringFirstNumberGTE"/></span>
<span class="js-criteria-title-and" style="display:none;"><g:message code="criteria.and"/></span>
<span class="js-criteria-title-or" style="display:none;"><g:message code="criteria.or"/></span>				
</strong></div>
				<div class="span4 text-right">
					<div style="display:none;" class="btn-group js-add-button">
						<button class="btn btn-mini dropdown-toggle" data-toggle="dropdown">
							<g:message code="criteria.add"/> <span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li><a lm-type="containsAnyPhrase" href="javascript:void(0);"><g:message code="criteria.containsAnyPhrase"/></a></li>
							<li><a lm-type="containsAllPhrase" href="javascript:void(0);"><g:message code="criteria.containsAllPhrase"/></a></li>
							<li><a lm-type="lineStartsWithSubstring" href="javascript:void(0);"><g:message code="criteria.lineStartsWithSubstring"/></a></li>
							<li><a lm-type="lineStartsWithSubstringContainsAnyPhrase" href="javascript:void(0);"><g:message code="criteria.lineStartsWithSubstringContainsAnyPhrase"/></a></li>
							<li><a lm-type="lineStartsWithSubstringFirstNumberGTE" href="javascript:void(0);"><g:message code="criteria.lineStartsWithSubstringFirstNumberGTE"/></a></li>
							<li><a lm-type="and" href="javascript:void(0);"><g:message code="criteria.and"/></a></li>
							<li><a lm-type="or" href="javascript:void(0);"><g:message code="criteria.or"/></a></li>
						</ul>
					</div>
					<button type="button" style="display:none;" title="${message(code:'criteria.negate.title')}" class="btn btn-mini js-negate-button">
						<g:message code="criteria.negate"/>
					</button>
					<a disabled title="${message(code:'criteria.delete.title')}" class="btn btn-mini js-delete-button" href="javascript:void(0);">x</a>
				</div>
			</div>				
		<hr/>
		<div class="js-criteria-content">
			<div class="js-criteria-content-and js-criteria-content-or" style="display: none;">
				
			</div>
			<div class="js-criteria-content-containsAllPhrase js-criteria-content-containsAnyPhrase" style="display: none;">
				<input lm-data-role="tagsinput" placeholder="${message(code:'criteria.placeholder.phrases')}" js-validation="required no-single-chars" />
			</div>
			<div class="js-criteria-content-lineStartsWithSubstringContainsAnyPhrase" style="display: none;">
				<input type="text" placeholder="Substring" class="substring" js-validation="required no-single-chars" />
				<input lm-data-role="tagsinput" placeholder="${message(code:'criteria.placeholder.phrases')}" js-validation="required no-single-chars" />
			</div>
			<div class="js-criteria-content-lineStartsWithSubstringFirstNumberGTE" style="display: none;">
				<input type="text" placeholder="Substring" class="substring" js-validation="required no-single-chars" />
				<input type="number" placeholder="Compare to" class="substring" js-validation="required number" />
			</div>
			<div class="js-criteria-content-lineStartsWithSubstring" style="display: none;">
				<input type="text" placeholder="Substring" class="substring" js-validation="required no-single-chars" />
			</div>
		</div>
	</div>
</div>

<%--
<button type="button" onclick="lm.criteria.showJson();">Validate/Show json</button>
<div id="jsonContent"></div>
 --%>