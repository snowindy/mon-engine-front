<div class="row">

	<div class="span6">
		<div class="about-block vertical-block">
			<h2>
				<g:message code="home.about.header" />
			</h2>
			<p>
				<g:message code="home.about.description" />
			</p>
		</div>
	</div>

	<div class="span6">
		<div>
			<h2>
				<g:message code="home.idea.header" />
			</h2>

			<table class="table dashed-vertical" id="idea-table">
				<g:message code="home.idea.description" />

			</table>

		</div>
	</div>
</div>