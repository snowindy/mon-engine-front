<%@page defaultCodec="none"%>
<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap" />
<title><g:message code="home.title.payment" /></title>
</head>

<body>
	<header>
		<g:render template="/shared/headerSecondaryPage" />
	</header>

	<div class="container" id="payment-form">
		<g:render template="/shared/flashMessage" />

		<div class="row padded">
			<div class="span6 col-centered" id="countdown">
				<h3><g:message code="payment.header" /></h3>

				<form class="form-inline smaller-margin" role="form">
					<div class="form-group">
						<label for="selectedDays"><g:message code="payment.days.label" /></label>
						<input	type="email" class="form-control small-input" id="selectedDays"
							placeholder="${message(code:'payment.days.label')}" value="${selectedDays}" />
					</div>
				</form>
				<p>
					<g:set var="p1">
						<span id="daysSelectedText">${selectedDays}</span>
					</g:set>
					<g:set var="p2">
						<strong><span id="price">${dayPrice * selectedDays}</span></strong>
					</g:set>
					<g:message code="payment.price.text" args="[p1,p2]" encodeAs="none"/>
				</p>
				<p>
					<a id="proceedButton" href="#"
						class="btn btn-success disabled" role="button"><g:message code="payment.button.label"/></a>
				</p>
			</div>
		</div>
		
		<hr/>

		<div class="row padded">
			<div class="span6 col-centered" id="countdown">
				<g:message code="payment.notice.text" args="[meta(name: 'company.url')]"/>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		var lm = lm||{};
		lm.payment = {};
		lm.payment.dayPrice = ${dayPrice};
		lm.payment.publicKey = '${publicKey}';
		lm.payment.user = '${user}';
		lm.payment.currency = '${currency}';		
	</script>
</body>
</html>
