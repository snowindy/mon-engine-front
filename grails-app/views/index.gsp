<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap" />
<title><g:message code="home.title.${session.loggedIn?'loggedIn':'notLoggedIn'}" /></title>
</head>

<body>

	<header>
		<g:render template="warningAnnouncement" />
		<g:render template="goodNewsAnnouncement" />
		<g:render template="adsAnnouncement" />
		<g:render template="/shared/headerSecondaryPage" />
	</header>

	<div class="container js-main-form-page">
		<g:render template="/shared/flashMessage" />
		<g:render template="/shared/validationErrors" model="[domains:[user, loginCmd]]" />
		
		<g:if test="${session.loggedIn}">
			<g:render template="form" />
			<g:render template="paymentDetails" />
			<g:render template="recentActivity" />
		</g:if>
		<g:else>
			<g:render template="intro" />
			<g:render template="form" />
			<g:render template="/shared/social/body" />
			<g:render template="recentActivity" />
		</g:else>
	</div>
</body>
</html>
