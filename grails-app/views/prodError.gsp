<!doctype html>
<html>
<head>
<title>Error occured, we're sorry</title>
<meta name="layout" content="bootstrap">
</head>
<body>
	<div class="container">
		<h1>An unexpected error occured</h1>
		<p>The admin team has been notified.</p>
		<p>
			Please try again later, or <a
				href="mailto:${meta(name: 'contact.email.userErrorPage')}?subject= [${GrailsUtil.environment}] Application Error occured : '${exception?.message?.encodeAsHTML()}'">
				contact </a> the IT team.
		</p>

	</div>

</body>
</html>