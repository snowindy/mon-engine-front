<%@ page import="org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><g:layoutTitle default="${meta(name: 'app.name')}" /></title>
<meta name="description" content="">
<meta name="author" content="">

<meta name="viewport" content="initial-scale = 1.0">

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

<r:require modules="application" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
<link rel="apple-touch-icon" sizes="72x72" href="${resource(dir: 'images', file: 'apple-touch-icon-72x72.png')}">
<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-114x114.png')}">

<g:layoutHead />
<r:layoutResources />
</head>

<body>
	<g:render template="/shared/externalSrv/googleTagManager" />
	<eg:noValidationFormSupport>
		<g:layoutBody />
	</eg:noValidationFormSupport>
	<hr />

	<footer>
		<div class="container">
			<p>
				&copy; <a href="${meta(name: 'company.url')}" title="${meta(name: 'company.name')}">
					${meta(name: 'company.name')}
				</a>
				${new Date().format('yyyy')}
			</p>
		</div>
	</footer>
	<r:layoutResources />
</body>
</html>