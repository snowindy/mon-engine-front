<!doctype html>
<html>
	<head>
		<title>Grails Runtime Exception [Dev mode error page]</title>
		<meta name="layout" content="bootstrap">
	</head>
	<body>
		<g:renderException exception="${exception}" />
	</body>
</html>