<%@ page contentType="text/html"%>
<strong>Password reset request has been issued.</strong><br/><br/>

To reset password follow the link:<br/>
<strong><a href="http://lancemonitor.com/passwordReset/newPassword?token=${token.uuid}">http://lancemonitor.com/passwordReset/newPassword?token=${token.uuid}</a></strong>
<br/>

<hr/>
<i>Blazing fast, <a href="http://lancemonitor.com">LanceMonitor.com</a></i>
