<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap" />
<title><g:message code="passwordReset.request.title" /></title>
</head>

<body>
	<g:render template="/shared/headerSecondaryPage" />

	<div class="container">
		<g:render template="/shared/flashMessage" />
		<g:render template="/shared/validationErrors" model="[domains:[passwordResetCmd]]" />

		<h3>
			<g:message code="resetPassword.request.form.header" />
		</h3>
		<g:form>
			<div class="row">
				<div class="span6">
					<p>
						<g:message code="resetPassword.request.form.message" />
					</p>
					<p>
						<g:link controller="root">
							<g:message code="common.to.home.page" />
						</g:link>
					</p>

				</div>
				<div class="span4">
					<f:field input-class="span4" bean="passwordResetRqCmd" property="email" />

					<g:actionSubmit class="btn btn-large btn-block btn-primary" action="resetPasswordRequest"
						value="${message(code:"resetPassword.form.reset.label")}" />
				</div>
			</div>
		</g:form>


	</div>
</body>
</html>
