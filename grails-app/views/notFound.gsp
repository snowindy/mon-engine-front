<!doctype html>
<html>
<head>
<title>Page not found</title>
<meta name="layout" content="bootstrap">
</head>
<body>
	<div class="container">
		<h1>Page not found</h1>
		<p>Page you requested is not found, please check the URL.</p>
	</div>
</body>
</html>