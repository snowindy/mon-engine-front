package com.github.snowindy.mon.front

import grails.util.Environment

import org.apache.commons.lang3.exception.ExceptionUtils
import org.codehaus.groovy.grails.commons.GrailsApplication
import org.codehaus.groovy.grails.web.errors.GrailsWrappedRuntimeException
import org.springframework.http.HttpStatus

import com.github.snowindy.mon.api.ex.RecordNotFoundException
import com.github.snowindy.mon.api.ex.ValidationException

class ErrorsController {

    GrailsApplication grailsApplication

    def mailService

    def serverError = {
        if (!handledError(request.exception)){
            log.error("Application error", request.exception)

            if(Environment.current == Environment.PRODUCTION){
                sendEmailReport(request.exception)
                render(view:'/prodError')
            } else {
                render(view:'/error')
            }
        } else {
            log.debug("Application handled error", request.exception)
        }
    }

    private boolean handledError(exception){
        def res = false
        if (exception instanceof GrailsWrappedRuntimeException){
            def ex = (GrailsWrappedRuntimeException)exception
            exception = ex.getCause()
            
            log.debug("Exception: " + exception.getClass())
            
            if (exception instanceof RecordNotFoundException){
                response.sendError(HttpStatus.NOT_FOUND.value)
                res = true
            } else if (exception instanceof ValidationException){
                response.sendError(HttpStatus.BAD_REQUEST.value)
                res = true
            }
        }
        
        res
    }

    def sendEmailReport(exception){
        try{
            def str = ExceptionUtils.getStackTrace(exception)
            def sbj = "[LanceMonitor ${Environment.current}] Error : '${exception?.message}'"
            def bd = """Error ${exception?.message} at ${new Date()}:\n\n${str}"""

            def recepinent = meta(name: 'contact.email.userErrorPage')

            mailService.sendMail {
                to recepinent
                from grailsApplication.config.mail.from
                replyTo grailsApplication.config.mail.replyTo
                subject sbj
                body bd
            }
        }catch(e){
            log.error("Unable to send email error report", e)
        }
    }

    def notFound = { render(view:'/notFound') }
}
