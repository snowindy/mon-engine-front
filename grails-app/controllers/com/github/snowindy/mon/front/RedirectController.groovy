package com.github.snowindy.mon.front

class RedirectController {
    def index() {
       redirect controller: params.destinationController
    }
}