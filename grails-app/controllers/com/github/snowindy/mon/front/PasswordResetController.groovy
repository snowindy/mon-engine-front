package com.github.snowindy.mon.front

import org.codehaus.groovy.grails.commons.GrailsApplication

import com.github.snowindy.mon.entity.Token
import com.github.snowindy.mon.front.cmd.PasswordResetCommand
import com.github.snowindy.mon.front.cmd.PasswordResetRequestCommand

class PasswordResetController {

    static allowedMethods = [resetPassword: ['POST'], resetPasswordRequest: ['POST']]

    UserService userService
    
    def mailService
    
    GrailsApplication grailsApplication

    def index = {
        PasswordResetRequestCommand passwordResetRqCmd = new PasswordResetRequestCommand()
        [passwordResetRqCmd: passwordResetRqCmd]
    }

    def newPassword = {
        PasswordResetCommand passwordResetCmd = new PasswordResetCommand(token:params.token)
        [passwordResetCmd: passwordResetCmd]
    }

    def resetPassword = { PasswordResetCommand passwordResetCmd ->
        def model = [passwordResetCmd:passwordResetCmd]

        if (!passwordResetCmd.validate()){
            render view: 'newPassword', model: model
            return
        }

        userService.updateUserPasswordWithToken(passwordResetCmd.token, passwordResetCmd.password)

        boolean res = true
        flash.message = message(code:'password.reset.operation.success')

        // passwordResetRqStatus is used for Google Analytics indication
        redirect(controller: 'root', params:[passwordResetRqStatus:res])
    }

    def resetPasswordRequest = { PasswordResetRequestCommand passwordResetRqCmd ->
        def model = [passwordResetRqCmd:passwordResetRqCmd]

        if (!passwordResetRqCmd.validate()){
            render view: 'index', model: model
            return
        }

        Token token = userService.createUserToken(passwordResetRqCmd.email)
        
        mailService.sendMail {
            to token.entityId
            from grailsApplication.config.mail.from
            replyTo grailsApplication.config.mail.replyTo
            subject "Freelance Job Monitor user password reset"
            body( view:"/passwordReset/mail/userPasswordResetBody", 
                model:[token:token])
        }

        flash.message = message(code:'password.reset.request.operation.success')
        boolean res = true

        // passwordResetRqStatus is used for Google Analytics indication
        redirect(controller: 'root', params:[passwordResetRqStatus:res])
    }
}

