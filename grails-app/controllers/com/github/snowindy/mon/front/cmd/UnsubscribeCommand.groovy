package com.github.snowindy.mon.front.cmd

import grails.validation.Validateable
import groovy.transform.ToString

import org.apache.commons.lang3.StringUtils

import com.github.snowindy.util.text.StringSplitUtils


@Validateable
@ToString
class UnsubscribeCommand {
    int timeoutHours
    String user

    static constraints = {
        user(blank:false)
        timeoutHours(blank:false, min: 1)
    }
}
