package com.github.snowindy.mon.front.cmd

import grails.validation.Validateable
import groovy.transform.ToString

import org.apache.commons.lang3.StringUtils

import com.github.snowindy.util.text.StringSplitUtils


@Validateable
@ToString
class PasswordResetCommand {
    String password
    String passwordConfirm
    
    String token
    
    // Only for the sake of whole instance validation
    String _instanceValidation

    static constraints = {
        password(size: 5..250, blank: false, password: true)
        passwordConfirm(blank:false, password: true)
        token(blank:false)
        _instanceValidation(nullable:true, validator: { val, obj, errors ->
            if (obj.password != obj.passwordConfirm){
                errors.rejectValue('password', 'user.password.doesnotmatch')
            }
        })
    }
}
