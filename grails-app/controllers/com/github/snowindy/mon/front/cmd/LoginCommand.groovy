package com.github.snowindy.mon.front.cmd

import grails.validation.Validateable

@Validateable
class LoginCommand {
    String login
    String password


    static constraints = {
        login(blank:false)
        password(blank:false)
    }
}
