package com.github.snowindy.mon.front

import javax.annotation.PostConstruct

import org.codehaus.groovy.grails.commons.GrailsApplication

class PlainFileController {

    GrailsApplication grailsApplication

    @PostConstruct
    def init(){
        if (!grailsApplication.config.plainFiles.directory){
            throw new RuntimeException("plain files directory is not specified")
        }
    }

    def index = {
        def fileName =  params.fileName +"." +params.extension
        log.info("Resolving file by name '$fileName'. Params: $params")

        File f = new File(grailsApplication.config.plainFiles.directory, fileName)
        f = f.absoluteFile
        if (f.exists()){
            log.info("Serving file '$f'.")
            render(contentType: "text/plain", text: f.getText("UTF-8"))
        }else{
            log.info("File not found '$f'.")
        }
    }
}
