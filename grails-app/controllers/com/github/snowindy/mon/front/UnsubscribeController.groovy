package com.github.snowindy.mon.front

import java.text.SimpleDateFormat

import com.github.snowindy.mon.entity.User
import com.github.snowindy.mon.front.cmd.UnsubscribeCommand

class UnsubscribeController {
    
    static allowedMethods = [unsubscribe: ['POST'], subscribe: ['GET'], deactivate: ['GET']]
    
    UserService userService
    
    def index = { UnsubscribeCommand unsubscribeCmd ->
        [unsubscribeCmd: unsubscribeCmd]
    }
    
    private static final MYSQL_TIMESTAMP_ERA_LAST_DATE = new SimpleDateFormat('yyyy-MM-dd').parse('2038-01-19') 
    
    def deactivate = {
        def userUuid = params.user
        
        boolean res = false
        
        if (userUuid){
            User user = userService.deactivateUser(userUuid)

            flash.message = message(code:'unsubscribe.deactivation.operation.success')
            res = true
            
            if (session.loggedIn){
                session.user = user
            }
        }else{
            log.warn("uuid not specified for deactivation")
            flash.message = message(code:'unsubscribe.deactivation.emailMissing.message')
        }
        
        // deactivationStatus is used for Google Analytics indication
        redirect(controller: 'root', params:[deactivationStatus:res])
    }

    def unsubscribe = { UnsubscribeCommand unsubscribeCmd ->
        def model = [unsubscribeCmd:unsubscribeCmd]

        if (!unsubscribeCmd.validate()){
            render view: 'index', model: model
            return
        }
        
        User user = User.findByUuid(unsubscribeCmd.user)        
        user.noSearchTill = new Date(System.currentTimeMillis() + unsubscribeCmd.timeoutHours*60L*60*1000)
        
        if (user.noSearchTill >= MYSQL_TIMESTAMP_ERA_LAST_DATE){
            user.noSearchTill = MYSQL_TIMESTAMP_ERA_LAST_DATE
        }
        
        user = userService.updateUser(user.id, user)
        session.user = user
        
        params.messages = params.messages?:[]
        params.messages << message(code:'unsubscribe.operation.success', args:[unsubscribeCmd.timeoutHours])

        render view:'index', model:model
    }

    def subscribe = {
        User user = User.findByUuid(params.user)
        
        user.noSearchTill = null
        user.active = true
        
        user = userService.updateUser(user.id, user)
        session.user = user
        
        flash.message = message(code:'subscribe.operation.success')
        
        redirect(controller: 'root')
    }

}