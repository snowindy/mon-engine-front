package com.github.snowindy.mon.front

import com.github.snowindy.mon.entity.MonItemFront
import com.github.snowindy.mon.front.ProxyService.ProxyServiceResult

class ProxyController {

    ProxyService proxyService

    def index() {
        def monItemUuid = params.item
        def userUuid = params.user

        ProxyServiceResult result = proxyService.handleProxyHit(monItemUuid, userUuid)

        if (result.usePlainRedirect){
            log.debug("Redirecting user '${result.user?.email}' to url '${result.monItem.url}'")
            redirect(url:result.monItem.url)
        }else{
            log.debug("Rendering wait dialog for user '${result.user?.email}' and url '${result.monItem.url}'")
            render(view:"index",model:[
                proxyServiceResult:result, 
                nextHopUrl:
                    createLink(controller:'proxy', action: 'afterWait', params: [user:userUuid, item:monItemUuid]),
                forceWatchPage: result.forceWatchPage
            ])
        }
    }

    def afterWait(){
        def monItemUuid = params.item
        def userUuid = params.user
        
        ProxyServiceResult result = proxyService.handleProxyHitAfterWait(monItemUuid, userUuid)

        log.debug("Redirecting user '${result.user?.email}' to url '${result.redirectUrl}' after wait")
        redirect(url:result.redirectUrl)
    }
}
