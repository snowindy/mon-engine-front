

package com.github.snowindy.mon.front

import groovy.json.JsonBuilder

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.codehaus.groovy.grails.commons.GrailsApplication

import com.github.snowindy.mon.entity.Payment
import com.github.snowindy.mon.entity.User

class PaymentController {

    GrailsApplication grailsApplication

    def index = {
        def model = [user: params.user, selectedDays: params.selectedDays?:30, dayPrice: grailsApplication.config.payment.dayPrice,
            publicKey: grailsApplication.config.payment.paygate.publicKey, currency: grailsApplication.config.payment.currency]
        
        render(view:"index", model:model)
    }
    
    def pay = {
        def respJson = [:]
        try{
            def jsonParams = new JsonBuilder(params).toString()
            log.info("Payment request '$jsonParams'.")
            
            checkSignature(params)
            
            def userUuid = params['params[account]']
            def orderCurrency = params['params[orderCurrency]']
            BigDecimal amount = new BigDecimal(params['params[orderSum]'])
            
            User user = User.findByUuid(userUuid)
            if (user == null){
                throw new IllegalArgumentException("User not found with id '$userUuid'")
            }
            if (amount <= 0){
                throw new IllegalArgumentException("Amount should be positive")
            }
            if (orderCurrency != 'USD'){
                throw new IllegalArgumentException("Expecting currency USD")
            }
            
            def dayPrice = grailsApplication.config.payment.dayPrice / 1.0
            def msPrice = dayPrice / (24*60*60*1000 as double)
            
            def mSecsToAdd = (amount / msPrice)  as long
            
            Date prevPaidTill = user.paidTill
            
            user.paidTill = user.paidTill?:new Date()
            user.paidTill = new Date().after(user.paidTill)? new Date(): user.paidTill
            user.paidTill = new Date(user.paidTill.time + mSecsToAdd)
            
            log.info("Payment '$params.method' millis to add '$mSecsToAdd' (${mSecsToAdd/(1000*60*60*24)} hours): prev paid till: '$prevPaidTill', new paid till: '$user.paidTill'.")
     
            if (params.method == 'pay'){
                Payment p = new Payment(userUuid: userUuid, params: jsonParams, amount: amount)
                p.save(failOnError:true, flush:true)
                
                user.save(failOnError:true, flush:true)            
            }else{
                user.discard()
            }
        
            respJson.result = ["message":"Successfully handled request"]
        }catch(e){
            log.error("Payment method error", e)
            respJson.error = ["code": -32000, "message": ExceptionUtils.getRootCauseMessage(e)]
        }
        
        render new JsonBuilder(respJson).toString()
    }

    private checkSignature(Map params) {
        def privateKey = grailsApplication.config.payment.paygate.privateKey

        def signature = params['params[sign]']
        def paramNamesSorted = params.collect{it.key}.findAll{(it.startsWith('params') && it != 'params[sign]')}.sort()
        def paramValConcat = paramNamesSorted.collect{params[it]}.join()
        paramValConcat = paramValConcat + privateKey

        def signCheck = paramValConcat.encodeAsMD5()

        if (signCheck != signature){
            throw new IllegalArgumentException("Signature does not match")
        }
    }
}
