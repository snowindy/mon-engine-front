package com.github.snowindy.mon.front

import com.github.snowindy.mon.entity.MonItem
import com.github.snowindy.mon.entity.MonItemFront

import com.github.snowindy.mon.api.ex.RecordNotFoundException

class MonItemController {

    def index = {
        log.info("Resolving monItem by uuid '$params.itemUuid'. Params: $params")

        def item = MonItemFront.findByUuid(params.itemUuid)
        
        if (!item){
            throw new RecordNotFoundException()
        }
        
        MonItem mi = new MonItem()
        mi.getMetadata().putAll(item.metadata)
        mi.setTitle(item.title)
        mi.setTextBody(item.textBody)
        
        mi.plainTextForm
        
        render(contentType: "text/plain", text: mi.plainTextForm)
    }
}
