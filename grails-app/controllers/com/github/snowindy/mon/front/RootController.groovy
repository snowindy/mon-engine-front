package com.github.snowindy.mon.front

import java.util.regex.Pattern

import org.apache.commons.lang.math.RandomUtils
import org.apache.commons.lang3.RandomStringUtils
import org.codehaus.groovy.grails.commons.GrailsApplication

import com.github.snowindy.mon.api.ex.DataUniquenessException
import com.github.snowindy.mon.api.ex.RecordNotFoundException
import com.github.snowindy.mon.entity.House
import com.github.snowindy.mon.entity.MonItemFront
import com.github.snowindy.mon.entity.User
import com.github.snowindy.mon.front.cmd.LoginCommand
import com.github.snowindy.util.grails.GrailsUtils

class RootController {

    static final String MODE_NEED_LOGIN_FOR_OPERATION = 'NEED_LOGIN_FOR_OPERATION'
    static final String PARAM_REDIRECT_AFTER_LOGIN = 'redirectAfterLogin'
    static final String PARAM_MODE = 'mode'

    static allowedMethods = [processNotificationRequest: ['POST'], index: ['GET', 'POST'], signIn: ['POST']]

    GrailsApplication grailsApplication
    ViewSupportService viewSupportService
    UserService userService

    def index() {
        initSuspendWarning(session, params)
        def model = [:]
        GrailsUtils.propagateParamToModel(model, params, PARAM_REDIRECT_AFTER_LOGIN)

        params.messages = params.messages?:[]
        if (params.mode == MODE_NEED_LOGIN_FOR_OPERATION){
            params.messages << message(code: 'home.login.needLoginForOperation.message')
        }
        if (params.paymentAcceptedSuccess != null){
            params.messages << message(code:'payment.operation.success')
        } else if (params.paymentAcceptedFailure != null){
            params.messages << message(code:'payment.operation.failure')
        }

        updateModelWithIndexViewData(model)
        if (session.loggedIn){
            model.user = session.user
        }else{
            model.user.housesOfInterest = model.houses
            model.user.query = ''
        }
        render(view:"/index",model:model)
    }
    
    def initSuspendWarning(session, params){
        if (session.loggedIn){
            if (session.user.noSearchTill){
                Date now = new Date()
                if (now.before(session.user.noSearchTill)){
                    int hours = getHoursBetween(session.user.noSearchTill, now)
                    int minutes = getMinutesBetween(session.user.noSearchTill, now)
                    minutes = minutes - 60*hours
                    params.messages = params.messages?:[]
                    params.messages << message(code:'warning.suspended.subscription',
                    args:[
                        hours,
                        minutes,
                        createLink(controller:'unsubscribe', action:'subscribe', params: [user: session.user.uuid])
                    ])
                }
            }
        }
    }

    // TODO should be parametrized, not hardcoded
    // TODO should be in a service layer
    private def isGlobalMarketplace = { House house ->
        (house.domainName == 'odesk.com' 
        || house.domainName == 'elance.com' 
        || house.domainName == 'guru.com'  
        || house.domainName == 'freelancer.com' 
        || house.domainName == 'peopleperhour.com'
        || house.domainName == 'getacoder.com')
    }

    private def updateModelWithIndexViewData(model){
        def houses = getHouses()
        def countStats = getHouseStatistics(houses)
        def recentMonItems = getRecentMonItems()
        def recentMonItemTimes = getMonItemTimes(recentMonItems)

        model.houses = houses
        model.globalHouses = houses.findAll(isGlobalMarketplace)
        model.localHouses = houses.findAll{ !isGlobalMarketplace(it) }
        model.recentMonItems = recentMonItems
        model.recentMonItemTimes = recentMonItemTimes
        model.countStats = countStats
        model.loginCmd = model.loginCmd?:new LoginCommand()
        if (!model.user){
            model.user = new User()
        }
        
        if (session.loggedIn){
            // Reload session user from DB
            session.user = User.findByUuid(session.user.uuid)
            
            Pattern emailPattern = Pattern.compile(grailsApplication.config.proxy.email.match.regex, Pattern.MULTILINE
                | Pattern.CASE_INSENSITIVE)
            if (emailPattern.matcher(session.user.email?:'').matches()){
                model.showPaymentDetails = true
            }
        }
    }

    private def getHouseStatistics(houses){
        def countMap = [:]
        houses.each{ House h ->
            countMap[h.domainName] = viewSupportService.getHouseItemCountForPeriod(h)
        }
        
        // Desc sort for marketplaces by value
        countMap = countMap.sort { -it.value }

        countMap
    }

    private def getHouses(){
        def houses = viewSupportService.getHouses()
        houses
    }

    private def getMonItemTimes(recentMonItems){
        Date now = new Date()
        def timeMap = [:]
        recentMonItems.each { MonItemFront item ->
            timeMap.put(item, getTimeBetweenAsText(item, now))
        }
        timeMap
    }

    private def getRecentMonItems(){
        def recentMonItems = viewSupportService.getRecentMonItems()
        recentMonItems = recentMonItems[0 ..< Math.min(recentMonItems.size(),15)]
        recentMonItems
    }

    // TODO move to utils
    private long getSecondsBetween(Date d2, Date d1){
        long val = (d2.getTime() - d1.getTime())/1000;
        val
    }

    // TODO move to utils
    private long getHoursBetween(Date d2, Date d1){
        long val = (d2.getTime() - d1.getTime())/(60*60*1000);
        val
    }

    // TODO move to utils
    private long getMinutesBetween(Date d2, Date d1){
        long val = (d2.getTime() - d1.getTime())/(60*1000);
        val
    }

    private def getTimeBetweenAsText(MonItemFront item, Date now){
        // TODO: i18n message
        // TODO: consider to find a time-text library
        long seconds = getSecondsBetween(now, item.dateDetected)

        String prefix

        if (seconds < 60){
            prefix = "$seconds seconds"
        }else{
            int minutes = seconds / 60
            seconds = seconds % 60

            if (minutes == 1){
                prefix = "$minutes minute $seconds seconds"
            }else{
                prefix = "$minutes minutes $seconds seconds"
            }

        }

        "$prefix ago"
    }


    def processNotificationRequest() {
        User user = new User(params)

        def model = [user:user]
        updateModelWithIndexViewData(model)
        
        boolean captchValid = session.loggedIn? true: isCaptchaValid()

        if (captchValid){
            try{
                if (session.loggedIn){
                    user.email = session.user.email
                    user.password = session.user.password
                    
                    if (user.validate()){
                        session.user = userService.updateUser(session.user.id, user)

                        flash.message = message(code: 'success.userUpdated.message')
                        redirect controller: 'root'
                        return
                    }
                }else{
                    if (user.validate()){
                        userService.createUser(user)

                        session.user = user
                        session.loggedIn = true

                        // A small hack removing captcha message
                        session["recaptcha_error"] = null

                        flash.message = message(code: 'success.userCreated.message')
                        redirect controller: 'root'
                        return
                    }
                }
            }catch (DataUniquenessException e) {
                params.warnings = params.warnings?:[]
                params.warnings << message(code: 'error.userAlreadyExist.message')
            }catch (e){
                log.error("Cannot perform user operation", e)
                params.warnings = params.warnings?:[]
                params.warnings << message(code: 'error.internalError.message')
            }
        }

        render view: '/index', model: model
    }

    def recaptchaService

    private boolean isCaptchaValid(){
        boolean recaptchaOK = true
        if (recaptchaService.isEnabled() && !session.captchaGuessOk){
            if (!recaptchaService.verifyAnswer(session, request.getRemoteAddr(), params)) {
                recaptchaOK = false
                params.validationErrors = params.warnings?:[]
                params.validationErrors << message(code: 'error.incorrectCaptcha.message')
            }else{
                // User has already answered captcha correctly for this session. We don't need to bother him with second guess.
                session.captchaGuessOk = true
            }
        }

        recaptchaOK
    }

    def signIn = { LoginCommand loginCmd ->

        def model = [loginCmd:loginCmd]
        updateModelWithIndexViewData(model)
        GrailsUtils.propagateParamToModel(model, params, PARAM_REDIRECT_AFTER_LOGIN)

        if (!(loginCmd.validate())){
            render view: '/index', model: model
            return
        }

        User user

        try{
            user = userService.getUserWithAuth(loginCmd.login, loginCmd.password)
            session.user = user
            session.loggedIn = true

            if (params[PARAM_REDIRECT_AFTER_LOGIN] && params[PARAM_REDIRECT_AFTER_LOGIN] != 'null'){

                def url = URLDecoder.decode(params[PARAM_REDIRECT_AFTER_LOGIN], 'UTF-8')
                redirect(url:url)
            }else{
                redirect controller: 'root'
            }
            return
        }catch (RecordNotFoundException e) {
            params.warnings = params.warnings?:[]
            params.warnings << message(code: 'error.couldNotAuthenticate.message')
        }

        render(view:"/index", model:model)
    }

    def signOut() {
        session.user = null
        session.loggedIn = false
        session.captchaGuessOk = false

        redirect controller: 'root'
    }

    private setActive(boolean active){
        User user = new User()
        GrailsUtils.copyProperties(session.user, user)
        
        user.active = active

        session.user = userService.updateUser(session.user.id, user)

        flash.message = message(code: "success.activeStateUpdated.active.${active}.message")

        redirect controller: 'root'
    }

    def activateSubscription(){
        setActive(true)
    }

    def deactivateSubscription(){
        setActive(false)
    }
}
