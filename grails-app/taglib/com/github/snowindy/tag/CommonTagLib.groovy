package com.github.snowindy.tag

import static org.codehaus.groovy.grails.commons.GrailsClassUtils.getStaticPropertyValue

import org.apache.commons.lang3.StringUtils

import com.github.snowindy.util.grails.GrailsUtils

class CommonTagLib {

    def messageService

    static namespace = "eg"

    def formatDate = { attrs, body ->
        out << g.formatDate(date:attrs.date, format:'dd-MM-yyyy')
    }

    static boolean formNovalidate = StringUtils.isNotBlank(System.getProperty("form.novalidate"))

    def noValidationFormSupport = { attrs, body ->
        String bodyStr = body()
        if (formNovalidate){
            bodyStr = bodyStr.replaceAll("<form action=", '<form novalidate="true" action=');
        }
        out << bodyStr
    }
}
