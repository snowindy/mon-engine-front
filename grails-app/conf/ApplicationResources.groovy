def dev = grails.util.GrailsUtil.isDevelopmentEnv()

def cssFile = "bootstrap.css"
def cssminFile = "bootstrap.min.css"

modules = {
    scaffolding {
        // HACK: 'bootstrap-css' currently propagates responsive CSS for some buggy reason: https://github.com/groovydev/twitter-bootstrap-grails-plugin/issues/73
        //dependsOn 'bootstrap-css'
        dependsOn 'bootstrap-alert'
        resource id: 'bootstrap-css', url:[plugin: 'twitter-bootstrap', dir: 'css', file: (dev ? cssFile : cssminFile)], disposition: 'head', exclude:'minify'
        resource url: 'css/scaffolding.css'
    }
    application {
        dependsOn 'scaffolding','jquery'
        resource url:'js/json2.js'
        resource url:'js/bootstrap-tagsinput.js'
        resource url:'js/app.js'
        resource url:'css/app.css'
        resource url:'css/bootstrap-tagsinput.css'
       // resource url:'less/app-style.less',attrs:[rel: "stylesheet/less", type:'css'], bundle:'bundle_style'
    }
}