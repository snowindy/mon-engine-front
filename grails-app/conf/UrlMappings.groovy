class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: 'root', action:'index')
        "/payment"(controller: 'root', action:'payment')
        "/item/$itemUuid"(controller: 'monItem', action:'index')
        "/$fileName*.$extension"(controller:"plainFile")
        "500"(controller: "errors", action:"serverError")
        "404"(controller: "errors", action:"notFound")
        "405" {
            controller = 'redirect'
            destinationController = 'root'
        }
    }
}
