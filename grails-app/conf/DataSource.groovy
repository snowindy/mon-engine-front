dataSource {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    username = "monengine1"
    password = "monengine1"
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
    naming_strategy = 'com.github.snowindy.hibernate.UppercaseImprovedNamingStrategy'
}
// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "validate" // one of 'create', 'create-drop', 'update', 'validate', ''
            url = "jdbc:mysql://localhost:3306/monengine1?characterEncoding=UTF-8&useEncoding=true"
        }
    }
    test {
        dataSource {
            dbCreate = "validate"
            url = "jdbc:mysql://localhost:3306/monengine1?characterEncoding=UTF-8&useEncoding=true"
        }
    }
    production {
        dataSource {
            dbCreate = "validate"
            url = "jdbc:mysql://localhost:3306/monengine1?characterEncoding=UTF-8&useEncoding=true"
            pooled = true
            properties {
                maxActive = -1
                minEvictableIdleTimeMillis=180000
                timeBetweenEvictionRunsMillis=180000
                numTestsPerEvictionRun=3
                testOnBorrow=true
                testWhileIdle=true
                testOnReturn=true
                validationQuery="SELECT 1"
            }
        }
    }
}