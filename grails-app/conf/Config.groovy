import com.github.snowindy.mon.Constants

// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

grails.config.locations = [
    "classpath:${appName}-config.properties",
    "classpath:${appName}-config.groovy",
    "file:${userHome}/.grails/${appName}-config.properties",
    "file:${userHome}/.grails/${appName}-config.groovy"
]

if (System.properties["${appName}.config.location"]) {
    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
}

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [
    all:           '*/*',
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
    form:          'application/x-www-form-urlencoded',
    html:          [
        'text/html',
        'application/xhtml+xml'
    ],
    js:            'text/javascript',
    json:          [
        'application/json',
        'text/json'
    ],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    xml:           [
        'text/xml',
        'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = [
    '/images/*',
    '/css/*',
    '/js/*',
    '/plugins/*'
]

// The default codec used to encode data with ${}
grails.views.default.codec = "html" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

environments {
    development {
        grails.logging.jul.usebridge = true
    }
    production {
        grails.logging.jul.usebridge = false
        // TODO: grails.serverURL = "http://www.changeme.com"
    }
}

def appLogPackages = [
    'com.github.snowindy',
    'grails.app.services',
    'grails.app.controllers',
    'grails.app.service',
    'grails.app.controller'
]

def grailsLogPackages = [
    'org.codehaus.groovy.grails.web.servlet',
    // controllers
    'org.codehaus.groovy.grails.web.pages',
    // GSP
    'org.codehaus.groovy.grails.web.sitemesh',
    // layouts
    'org.codehaus.groovy.grails.web.mapping.filter',
    // URL mapping
    'org.codehaus.groovy.grails.web.mapping',
    // URL mapping
    'org.codehaus.groovy.grails.commons',
    // core / classloading
    'org.codehaus.groovy.grails.plugins',
    // plugins
    'org.codehaus.groovy.grails.orm.hibernate',
    // hibernate integration
    'org.springframework',
    'org.hibernate',
    'net.sf.ehcache.hibernate',
    'grails.app.services.org.grails.plugin.resource',
    'grails.app.taglib.org.grails.plugin.resource',
    'grails.app.resourceMappers.org.grails.plugin.resource',
    'grails.app.services.FormFieldsTemplateService',
    'grails.app.services.grails.plugin.formfields.FormFieldsTemplateService'
]

def logLayoutPattern = '%d [%t] %-5p %c: %m%n'

environments {
    development {
        log4j = {
            appenders {
                rollingFile name:'fileLog', file:"./target/mon-engine-front.log", maxFileSize:"50MB", maxBackupIndex:5, layout: pattern(conversionPattern: logLayoutPattern)
                console name: "stdout", layout: pattern(conversionPattern: logLayoutPattern)
            }

            root{
                additivity: true
                error()
            }
            info additivity: false, fileLog: grailsLogPackages
            all additivity: false, stdout: appLogPackages
        }
    }

    production {
        log4j = {
            appenders {
                rollingFile name:'fileLog', file:"../logs/mon-engine-front.log", maxFileSize:"50MB", maxBackupIndex:5, layout: pattern(conversionPattern: logLayoutPattern)
                console name: "stdout", layout: pattern(conversionPattern: logLayoutPattern)
            }

            root{
                additivity: true
                error()
            }
            info additivity: false, fileLog: grailsLogPackages
            debug additivity: false, fileLog: appLogPackages
        }
    }

    test {
        log4j = {
            appenders {
                console name: "stdout", layout: pattern(conversionPattern: logLayoutPattern)
            }

            all appLogPackages
        }
    }
}

grails.cache.config = {
    cache {
        name Constants.Cache.REST
        eternal false
        overflowToDisk false
        maxElementsInMemory 10000
    }
    cache {
        name Constants.Cache.REST_SNAPSHOT
        eternal false
        overflowToDisk false
        maxElementsInMemory 10000
    }
    cache {
        name Constants.Cache.REST_STATS
        eternal false
        overflowToDisk false
        maxElementsInMemory 10000
    }
    cache {
        name Constants.Cache.PROXY_WAITS
        eternal false
        overflowToDisk false
        maxElementsInMemory 1000
    }
    cache {
        name Constants.Cache.PROXY_FREEPASS
        eternal false
        overflowToDisk false
        maxElementsInMemory 100000
    }
}

cache.rest.evict.startDelaySecons=10
cache.rest.evict.repeatIntervalSecons=10*60
cache.rest.snapshot.evict.startDelaySecons=10
cache.rest.snapshot.evict.repeatIntervalSecons=20
cache.rest.stats.evict.startDelaySecons=10
cache.rest.stats.evict.repeatIntervalSecons=1*60
cache.rest.refresh.startDelaySecons=10
cache.rest.refresh.repeatIntervalSecons=1*30

stats.monItem.timePeriod = 24*60*60*1000

mail.from = "LanceMonitor <mailer@lancemonitor.com>"
mail.replyTo = "LanceMonitor <mailer@lancemonitor.com>"

keen.projectId = '533e1220ce5e43285200000c'
keen.writeKey = 'eb5f5ce880d6501520814cfaee53ae684966ec084a980a129fdb73f0fec4b98cf4ba7e5cfc6260b394c9ccfecc9a881143e88a9a7b6830b9d1f06484533030ca74e4e3c1f9d41a561187af72300dc2e57eaa308a6516ddc8c3950949172a2c4675373e4ee50695c5951eec043c66f061'
keen.readKey = '45fd9fad39e8231958f45fa8dc717ce35190dd9dfe8aca2887fb4733609babef831c6c1b286c15d3373826f6742ee9589e24c8cf1e5b7af8a2a2c519f4a4725cb2f5319a7b111eedb5906498578aa74b0ae2de3edfe0b474a5f8f7fb531739969b1191ac80a1a4835d59891c4ea8bfc6'

proxy.wait.seconds=20
proxy.email.match.regex='blackorangebox@gmail.com'
proxy.freePassViewsNumber=3

proxy.forceWatchPage.email.match.regex='blackorangebox@gmail.com'
proxy.forceWatchPage.easyPassDaysNumber=60

grails.mime.file.extensions = false
plainFiles.directory = "${userHome}/.grails/mon-engine-plain-files/"

payment.dayPrice=0.2
payment.paygate.publicKey='8469-f23dd'
payment.currency='USD'