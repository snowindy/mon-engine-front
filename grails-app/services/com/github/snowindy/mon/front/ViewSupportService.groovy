package com.github.snowindy.mon.front

import grails.plugin.cache.Cacheable

import com.github.snowindy.mon.Constants
import com.github.snowindy.mon.entity.House
import com.github.snowindy.mon.entity.MonItemFront
import com.github.snowindy.util.grails.GrailsUtils
import com.sun.org.apache.xalan.internal.xsltc.compiler.ForEach;


class ViewSupportService {

    static transactional = true

    def sessionFactory

    
    @Cacheable(Constants.Cache.REST)
    def getHouses(){
        House.findAllActive().collect{it}
    }

    
    @Cacheable(Constants.Cache.REST_SNAPSHOT)
    def getRecentMonItems(){
        MonItemFront.findAll("from MonItemFront m order by m.dateDetected desc", [max: 50, offset: 0])
    }
    
    @Cacheable(value = Constants.Cache.REST_STATS, key = "#house.domainName")
    def getHouseItemCountForPeriod(House house){

        def query = sessionFactory.currentSession
                .createQuery("select count(m) from MonItemFront m where m.dateDetected > ? and m.houseTopic.house = ?")

        Calendar cal = Calendar.getInstance()
        // TODO parametrize period. Current value is 24 hours
        cal.add(Calendar.DATE, -1)

        query.setCalendar(0, cal)
        query.setEntity(1, house)

        def count = query.uniqueResult()

        count
    }
}
