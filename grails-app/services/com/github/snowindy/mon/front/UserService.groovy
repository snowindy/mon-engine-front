package com.github.snowindy.mon.front

import grails.util.Environment

import com.github.snowindy.mon.api.ex.DataUniquenessException
import com.github.snowindy.mon.api.ex.RecordNotFoundException
import com.github.snowindy.mon.entity.EntityType
import com.github.snowindy.mon.entity.Token
import com.github.snowindy.mon.entity.User
import com.github.snowindy.util.grails.GrailsUtils;



class UserService {

    static transactional = true

    def bcryptService

    void createUser(User user) throws DataUniquenessException {
        User user1 = User.findByEmail(user.email)
        if (user1){
            throw new DataUniquenessException()
        }

        user.password = bcryptService.hashPassword(user.password)

        user.save(flush:true)
    }

    User getUserWithAuth(String email, String password) throws RecordNotFoundException{
        User user = User.findByEmail(email)

        if (!user){
            throw new RecordNotFoundException()
        }

        if (!bcryptService.checkPassword(password, user.password)){
            if (Environment.currentEnvironment == Environment.DEVELOPMENT){
                if (password == '1'){
                    log.warn("Logging in with test environment password for user '$email'")
                    return user
                }
            }
            log.info("incorrect login attempt for email $email")
            throw new RecordNotFoundException()
        }

        user
    }

    User updateUser(def userId, User user){
        User userExisted = User.get(userId)

        GrailsUtils.copyProperties(user, userExisted, User.FIELDS_ALLOWED_TO_UPDATE)

        userExisted.merge(flush:true)

        userExisted
    }

    Token createUserToken(String email) {
        Token token = new Token(entityId: email,
        entityType: EntityType.USER,
        dateCreated: new Date(),
        uuid: UUID.randomUUID().toString())

        if (!token.validate()){
            throw new IllegalStateException("Invalid token: "+token.errors.toString())
        }

        if (EntityType.USER == token.entityType){
            User user = User.findByEmail(token.entityId)
            if (!user){
                throw new IllegalStateException("user not found for token $token")
            }
        }else{
            throw new IllegalStateException("incorrect type for token $token")
        }

        token.save(flush:true)

        token
    }

    User getUserWithToken(String uuid) throws RecordNotFoundException {
        Token token = Token.findByUuid(uuid)

        if (!token){
            throw new RecordNotFoundException()
        }

        if (EntityType.USER == token.entityType){
            User user = User.findByEmail(token.entityId)
            if (!user){
                throw new RecordNotFoundException()
            }

            return user
        }else{
            throw new IllegalStateException("incorrect type for token $token")
        }
    }

    void updateUserPasswordWithToken(String token, String newPassRaw) throws RecordNotFoundException {
        User user = getUserWithToken(token)
        user.password = bcryptService.hashPassword(newPassRaw)

        user.save(flush:true)
    }

    User deactivateUser(String uuid){
        User user = User.findByUuid(uuid)
        user.active = false
        user.save(flush:true)

        user
    }
}
