package com.github.snowindy.mon.front

import io.keen.client.java.KeenClient
import io.keen.client.java.exceptions.KeenException

import java.util.regex.Pattern

import javax.annotation.PostConstruct

import org.codehaus.groovy.grails.commons.GrailsApplication
import org.springframework.cache.Cache
import org.springframework.cache.CacheManager

import com.github.snowindy.mon.Constants
import com.github.snowindy.mon.api.ex.RecordNotFoundException
import com.github.snowindy.mon.entity.MonItemFront
import com.github.snowindy.mon.entity.User
import com.github.snowindy.util.text.TextUtils;

class ProxyService {

    static transactional = true

    GrailsApplication grailsApplication

    CacheManager grailsCacheManager
    
    int DAY = 24*60*60*1000

    @PostConstruct
    private void init(){
        KeenClient.initialize(grailsApplication.config.keen.projectId, grailsApplication.config.keen.writeKey, grailsApplication.config.keen.readKey);
    }

    ProxyServiceResult handleProxyHit(String monItemUuid, String userUuid){
        ProxyServiceResult result = initProxyResult(monItemUuid, userUuid)

        if (!result.user){
            log.warn("User not found by uuid '$userUuid' for mon item '$monItemUuid'.")
            throw new RecordNotFoundException()
        }else{
            trackWithKeen(result.monItem, result.user)
        }
        
        result.usePlainRedirect = detectPlainRedirect(result)
        result.forceWatchPage = detectForceWatchPage(result)

        if (!result.usePlainRedirect){
            initCacheRecord(monItemUuid, userUuid, result)
        }
        
        result
    }
    
    private initCacheRecord(String monItemUuid, String userUuid, ProxyServiceResult result){
        Cache cache = grailsCacheManager.getCache(Constants.Cache.PROXY_WAITS)
        def key = formCacheKey(monItemUuid, userUuid)
        cache.put(key, new Date(System.currentTimeMillis() + result.waitSeconds*1000))
    }

    private ProxyServiceResult initProxyResult(String monItemUuid, String userUuid) {
        MonItemFront item  = MonItemFront.findByUuid(monItemUuid)
        if (!item){
            throw new RecordNotFoundException()
        }

        ProxyServiceResult result = new ProxyServiceResult()
        result.monItem = item
        result.waitSeconds = grailsApplication.config.proxy.wait.seconds

        User user = User.findByUuid(userUuid)
        result.user = user
        return result
    }

    private def detectPlainRedirect(ProxyServiceResult result) {
        Pattern emailPattern = Pattern.compile(grailsApplication.config.proxy.email.match.regex, Pattern.MULTILINE
            | Pattern.CASE_INSENSITIVE)
        
        def usePlainRedirect = true
        
        if (emailPattern.matcher(result.user.email).matches()){
            usePlainRedirect = false

            if (result.user.paidTill){
                Date now = new Date()
                if (result.user.paidTill > now){
                    log.debug("User $result.user.email ($result.user.uuid) has paid access till '$result.user.paidTill', granted direct access.")
                    usePlainRedirect = true
                } else {
                    def eraDay = (long)Math.floor(now.time / DAY)
                    Cache cache = grailsCacheManager.getCache(Constants.Cache.PROXY_FREEPASS)
                    def key = result.user.uuid +":" +eraDay
                    def content = cache.get(key)
                    def numberOfViewsToday
                    if (content == null) {
                        numberOfViewsToday = 0
                    } else {
                        numberOfViewsToday = content.get()
                    }
                    log.debug("User $result.user.email ($result.user.uuid) numberOfViewsToday = $numberOfViewsToday, key = $key")
                    if (numberOfViewsToday < grailsApplication.config.proxy.freePassViewsNumber){
                        usePlainRedirect = true
                        log.debug("Granted free pass for user $result.user.email ($result.user.uuid)")
                    }
                    numberOfViewsToday++
                    cache.put(key, numberOfViewsToday)
                }
            }
        }else{
            log.debug("User email '$result.user.email' did not match proxy-on regex: '${grailsApplication.config.proxy.email.match.regex}'.")
        }
        
        usePlainRedirect
    }
    
    private def detectForceWatchPage(ProxyServiceResult result) {
        Pattern emailPattern = Pattern.compile(grailsApplication.config.proxy.forceWatchPage.email.match.regex, Pattern.MULTILINE
            | Pattern.CASE_INSENSITIVE)
        
        def forceWatchPage = false

        if (emailPattern.matcher(result.user.email).matches() && result.user.addedDate < new Date(System.currentTimeMillis() 
            - grailsApplication.config.proxy.forceWatchPage.easyPassDaysNumber*DAY)){
            forceWatchPage = true
        }else{
            log.debug("User email '$result.user.email' did not match forceWatchPage condition: '${grailsApplication.config.proxy.forceWatchPage.email.match.regex}'.")
        }
        
        forceWatchPage
    }

    private String formCacheKey(String monItemUuid, String userUuid){
        "mon-item-${monItemUuid}--user-${userUuid}"
    }


    ProxyServiceResult handleProxyHitAfterWait(String monItemUuid, String userUuid){

        ProxyServiceResult result = initProxyResult(monItemUuid, userUuid)

        Cache cache = grailsCacheManager.getCache(Constants.Cache.PROXY_WAITS)
        def key = formCacheKey(monItemUuid, userUuid)
        def content = cache.get(key)
        if (content == null) {
            log.warn("Cannot get time data for cache key '${key}'. Created cache record anew.")
            initCacheRecord(monItemUuid, userUuid, result)
            throw new RecordNotFoundException("You will have to wait again and then retry your request.")
        } else {
            Date date = content.get()
            Date now = new Date()
            if (date > now){
                log.warn("Threshold date '${date}' for key '${key}' is after current time '${now}'. Probably proxy hack attempt for user '${result.user?.email}'.")
                throw new RecordNotFoundException()
            }else{
                // all good
            }
        }

        result.redirectUrl = TextUtils.addParametersToUrl(result.monItem.url, ['utm_source': 'lancemonitor.com'])
        
        result
    }

    static class ProxyServiceResult{
        boolean usePlainRedirect = false
        boolean forceWatchPage = false
        int waitSeconds
        User user
        MonItemFront monItem
        String redirectUrl
    }

    void trackWithKeen(MonItemFront item, User user){
        log.debug("Tracking event for item $item.uuid and user $user.uuid")

        def event = [user: [
                email: user.email,
                uuid: user.uuid],
            item: [
                uuid: item.uuid,
                url: item.url,
                title: item.title,
                date: item.dateDetected,
                house: item.houseTopic.house.domainName
            ]
        ]

        try {
            KeenClient.client().addEvent("proxyHits", event);
        } catch (KeenException e) {
            log.warn(e)
        }
    }
}
