package com.github.snowindy.mon.front

import org.springframework.validation.BeanPropertyBindingResult

class CommonService {

    def messageService

    static transactional = false

    static def validateDomainsInternal(def onInvalidCallback, Object[] args){
        boolean res = true;

        if(args != null && args.size() > 0){
            args.each{
                if(it != null){
                    def valRes = it.validate()
                    res = valRes && res
                    if (!valRes && onInvalidCallback){
                        onInvalidCallback(it)
                    }
                }
            }

            return res
        }else{
            return false
        }
    }

    static def validateDomains(Object... args){
        validateDomainsInternal(null, args)
    }

    static def findInvalidDomains(Object... args){
        def invalid = []
        validateDomainsInternal({def invalidDomain -> 
            invalid << invalidDomain
        }, args)
        
        invalid
    }
    
    static def findInvalidDomains(List args){
        findInvalidDomains(args as Object[])
    }

    static def validateDomains(List args){
        validateDomains(args as Object[])
    }

    static boolean isValid(arg){
        if (arg instanceof List){
            if (arg){
                def invalidFound = arg.find{
                    it == null? false: it.errors.hasErrors()
                }
                !invalidFound
            }else{
                true
            }
        }else{
            if (arg != null){
                !arg.errors.hasErrors()
            } else{
                true
            }
        }
    }
}
