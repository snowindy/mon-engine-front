// String format: http://stackoverflow.com/questions/610406/javascript-equivalent-to-printf-string-format
if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

var lm = lm || {};
lm.log = function(arg){
	if (console){
		console.log(arg);
	}
};
(function($) {
	$('#spinner').ajaxStart(function() {
		$(this).fadeIn();
	}).ajaxStop(function() {
		$(this).fadeOut();
	});

	(function captchaNotShownInitially() {
		var $email = $('*[name="email"]');
		if (!$email.val()) {
			$('#recaptcha_widget').hide();

			var showCaptcha = function() {
				$('#main-submit').removeClass('top-offset');
				$('#recaptcha_widget').show('fast');
			}

			$email.focus(showCaptcha);
			$email.change(showCaptcha);
		} else {
			$('#main-submit').removeClass('top-offset');
		}
	})();

	(function wordFormHintNotShownInitially() {
		var $wf = $('#wordform-hint');
		$wf.hide();
		var $kw = $('.keywords');

		var showWordFormHint = function() {
			$wf.show('fast');
		}

		$kw.focus(showWordFormHint);
		$kw.change(showWordFormHint);
	})();

	(function analyticsLinkAttrSupport() {
		var $elWithAnalytics = $('[analytics-category]');

		$elWithAnalytics.click(function() {
			var $this = $(this);
			return trackOutboundLink(this, $this.attr('analytics-category'),
					$this.attr('analytics-action'));
		});
	})();

	(function paymentFormSupport() {
		var button = $('#payment-form #proceedButton');

		var refreshPrice = function($daysEl) {
			if ($.isNumeric($daysEl.val())) {
				var days = parseFloat($daysEl.val());
				var orderPrice = lm.payment.dayPrice * days;
				$('#payment-form #price').val(orderPrice);
				
				orderPrice = orderPrice.toFixed(2);

				button.removeClass('disabled');
				button.attr('href', "https://unitpay.ru/pay/{0}/card?sum={1}&account={2}&currency={3}&desc={4}"
						.format(lm.payment.publicKey, orderPrice, lm.payment.user, lm.payment.currency,
								encodeURIComponent("Payment for {0} days, LanceMonitor.".format(days))));

				$('#payment-form #daysSelectedText').text(days);
				$('#payment-form #price').text(orderPrice);
			}
		};

		$("#payment-form #selectedDays").on("change keyup paste click",
				function(){refreshPrice($(this))});

		refreshPrice($("#payment-form #selectedDays"));
	})();
	
	(function initSearchQuery() {
		var criteriaBuilder = $('#criteriasBuilder');
		
		//$('#criteriaSampleContainer input').tagsinput('destroy');
		
		var applyItemToContainer = function applyItem(item, container){
			lm.log("Adding item "+item.type);
			var criteria = $('#criteriaSampleContainer>div').clone();
			
			criteria.attr('lm-negated', item.type == 'not');
			if (item.type == 'not'){
				criteria.find('.js-criteria-negation').show();
				item = item.items[0];
				criteria.find('.js-negate-button').addClass('btn-warning');
			}
			criteria.attr('lm-type', item.type);
			
			if (!container.is(criteriaBuilder)){
				criteria.find('.js-delete-button').removeAttr('disabled');
				criteria.find('.js-negate-button').show();
			}
			container.append(criteria);
			var contentsContainer = criteria.find('.js-criteria-content-'+item.type);
			var title = criteria.find('.js-criteria-title-'+item.type);
			
			contentsContainer.show();
			title.show();
			
			if (item.type == 'and' || item.type == 'or'){
				criteria.find('.js-add-button').show();
				for(var i=0; i < item.items.length; i++){
					applyItem(item.items[i], contentsContainer);	
				}
			}else{
				var inputs = contentsContainer.find('input:visible');
				if (inputs.length == 2){
					if (item.items){
						$(inputs.get(0)).val(item.items[0]);
						$(inputs.get(1)).val(item.items.slice(1).join(','));
						
					}
				} else if (inputs.length == 1){
					inputs.val(item.items.join(','));
				} else {
					throw "Incorrect number of inputs was detected: "+inputs.length;
				}
				var unputsDataRole = inputs.filter('[lm-data-role]');
				unputsDataRole.removeAttr('lm-data-role');
				unputsDataRole.attr('data-role', 'tagsinput');
				unputsDataRole.tagsinput('refresh');
			}
		}
		
		if (criteriaBuilder.length){
			var container = $('#criteriasBuilder');
			if (!lm.query){
				lm.query = {"items":[],"type":"and"};
			}else{
				$('#emptyCriteriaHint').hide();
			}
			applyItemToContainer(lm.query, container);
		}
		
		$('#criteriasBuilder').on('click', '.js-negate-button', function(){
			var el = $(this);
			el.toggleClass('btn-warning');
			var criteria = el.closest('.js-criteria');
			criteria.find('.js-criteria-negation:eq(0)').toggle();
			criteria.attr('lm-negated', criteria.attr('lm-negated') != 'true');
		});
		$('#criteriasBuilder').on('click', '.js-delete-button', function(){
			var el = $(this);
			if (!el.attr('disabled')){
				var criteria = el.closest('.js-criteria');
				criteria.remove();	
			}
		});
		$('#criteriasBuilder').on('click', '.criteria .dropdown-menu a', function(){
			var el = $(this);
			var addType = el.attr('lm-type');
			var criteria = el.closest('.js-criteria');
			var item = {type: addType, items: []};
			applyItemToContainer(item, criteria.find('.js-criteria-content-'+criteria.attr('lm-type')+':eq(0)'));
		});
		
		
		
		var rootAndContainer = $('#criteriasBuilder .js-criteria-content-and:eq(0)');
		lm.criteria = lm.criteria||{};
		lm.criteria.addTitle = function(){
			applyItemToContainer({
				type:'lineStartsWithSubstringContainsAnyPhrase', 
				items: ['Title:']
			}, rootAndContainer);
		};
		lm.criteria.addSkills = function(){
			applyItemToContainer({
				type:'lineStartsWithSubstringContainsAnyPhrase', 
				items: ['Skills:']
			}, rootAndContainer);
		};
		lm.criteria.addCategory = function(){
			applyItemToContainer({
				type:'lineStartsWithSubstringContainsAnyPhrase', 
				items: ['Category:']
			}, rootAndContainer);
		};
		lm.criteria.addBudgetGTE = function(){
			applyItemToContainer({
				type:'lineStartsWithSubstringFirstNumberGTE', 
				items: ['Max budget:']
			}, rootAndContainer);
		};
		lm.criteria.addBudgetUnknownOrGTE = function(){
			applyItemToContainer({type: 'or',
				items: [
			        {
						type:'lineStartsWithSubstringFirstNumberGTE', 
						items: ['Max budget:']
			        },
					{
						type:'not', 
						items: [
						    {
						    	type:'lineStartsWithSubstring',
						    	items: ['Max budget:']
						    }
						]},
		        ]}, rootAndContainer);
		};
		lm.criteria.addSimplePhrases = function(){
			applyItemToContainer({
				type:'containsAnyPhrase', 
				items: []
	        }, rootAndContainer);
		};
		lm.criteria.addSimplePhrasesWithStop = function(){
			applyItemToContainer({
				type: 'and',
				items: [
					{
						type:'containsAnyPhrase', 
						items: []
					},
					{
						type:'not', 
						items: [
							{
								type:'containsAnyPhrase', 
								items: []
							}
						]
					}
		        ]
			}, rootAndContainer);
		};
		lm.criteria.addClientCountry = function(){
			applyItemToContainer({
				type:'lineStartsWithSubstringContainsAnyPhrase', 
				items: ['Country:']
			}, rootAndContainer);
		};
		lm.criteria.addHourlyRateUnknownOrGTE = function(){
			applyItemToContainer({type: 'or',
				items: [
			        {
						type:'lineStartsWithSubstringFirstNumberGTE', 
						items: ['Max rate:']
			        },
					{
						type:'not', 
						items: [
						    {
						    	type:'lineStartsWithSubstring',
						    	items: ['Max rate:']
						    }
						]},
		        ]}, rootAndContainer);
		};
		
		lm.criteria.getCriteriasJson = function(){
			function getInner(criteriaEl){
				var type = criteriaEl.attr('lm-type');
				var cont = criteriaEl.find('.js-criteria-content-'+type+':eq(0)');
				var item;
				var items;
				if (criteriaEl.attr('lm-negated') == 'true'){
					item = {type: 'not', items: [{type: type, items: []}]};
					items = item.items[0].items;
				}else{
					item = {type: type, items: []};
					items = item.items;
				}
				
				function addSplitToArr(str, arr){
					$.each(str.split(','), function(idx, val){
						arr.push(val);
					});
				}
				
				if (type == 'and' || type == 'or'){
					cont.children('.js-criteria').each(function(idx, el){
						items.push(getInner($(el)));
					});
				} else if (type == 'containsAnyPhrase' || type == 'containsAllPhrase'){
					addSplitToArr(cont.children('input').val(), items);
				} else if (type == 'lineStartsWithSubstring'){
					items.push(cont.children('input').val());
				} else if (type == 'lineStartsWithSubstringContainsAnyPhrase' || type == 'lineStartsWithSubstringFirstNumberGTE'){
					items.push(cont.children('input:eq(0)').val());
					addSplitToArr(cont.children('input:eq(1)').val(), items);
				}else {
					throw "Unknow type "+type;
				}
				return item;
			};
			
			return getInner($('#criteriasBuilder .js-criteria:eq(0)'));
		}
		
		lm.criteria.showJson = function(){
			$('#jsonContent').children().remove();
			var p = $('<p></p>');
			$('#jsonContent').append(p);
			p.text('valid = '+lm.criteria.validate());
			
			p = $('<p></p>');
			$('#jsonContent').append(p);
			p.text(JSON.stringify(lm.criteria.getCriteriasJson()));
		};
		
		lm.criteria.validate = function(){
			var globalValid = true;
			
			$('#criteriasBuilder .criteria .js-criteria-content>div:visible').each(function(idx, el){
				el = $(el);
				
				function markInvalid(){
					el.closest('.criteria').addClass('error');
					globalValid = false;
				}
				
				el.closest('.criteria').removeClass('error');
				
				
				var criteriaType = el.closest('.criteria').attr('lm-type');
				if (criteriaType == 'or' || criteriaType == 'and'){
					if (el.find('.criteria').length == 0){
						markInvalid();
					}
				}else{
					el.find('[js-validation]').each(function(idx, inp){
						inp = $(inp);
						var val = inp.val().trim();
						var vldRules = inp.attr('js-validation').split(' ');
						for(var i = 0; i < vldRules.length; i++){
							var vldRule = vldRules[i];
							if (vldRule == 'required'){
								if (!val){
									markInvalid();
									return;	
								}
							}else if (vldRule == 'no-single-chars'){
								if (val){
									var arr = val.split(',');
									for(var j = 0; j < arr.length; j++){
										var item = arr[j].trim();
										if (item.length < 1){
											markInvalid();
											return;
										}
									}	
								}
							}else if (vldRule == 'number'){
								if (val && isNaN(val)){
									markInvalid();
									return;	
								}
							}else{
								throw "Unknown validation rule '"+vldRule+"'";
							}
						}
					});	
				}
			});
			
			return globalValid;
		};
		
		$('#criteriasBuilderHolder a').click(function(){
			$('#emptyCriteriaHint').hide();
		});
	})();
	
	(function submitSupport() {
		var button = $('#main-submit');

		lm.profileUpdate = lm.profileUpdate || {};
		lm.profileUpdate.onSubmit = function(){
			if (lm.criteria.validate()){
				$('#formUserQueryInput').val(JSON.stringify(lm.criteria.getCriteriasJson()));
				return true;
			}
			return false;
		};
	})();
})(jQuery);

function trackOutboundLink(link, category, action) {
	try {
		ga('send', 'event', category, action);
	} catch (err) {
	}

	setTimeout(function() {
		document.location.href = link.href;
	}, 500);

	return false;
}

lm.initWaitScreen = function() {
	if ($('.js-proxy-page').length){
		// lm.proxy.nextHopUrl
		// lm.proxy.waitSeconds
		// lm.proxy.itemTitle HTML_ENC
		// lm.proxy.forceWatchPage

		var sTime = new Date().getTime();
		var countDown = lm.proxy.waitSeconds;
		
		var initialTitle = $('head title').text();
		
		var blurTime = null;
		
		var allowTimeUpdate = true;
		if (lm.proxy.forceWatchPage){
			jQuery(window).bind("focus",function(event){
				allowTimeUpdate = true;
				if (blurTime){
					var now = new Date().getTime();
					sTime += (now - blurTime);
					blurTime = null;	
				}
		    }).bind("blur", function(event){
		    	allowTimeUpdate = false;
		    	blurTime = new Date().getTime();
		    });
		}

		function waitComplete() {
			$("#countdown").hide();
			$("#aftercount").show();
			$("head title").text(initialTitle);

			var nextURL = "//" + window.location.host + lm.proxy.nextHopUrl;

			window.location = nextURL;
		}

		function updateTime() {
			if (allowTimeUpdate){
				var cTime = new Date().getTime();
				var diff = cTime - sTime;
				var seconds = countDown - Math.floor(diff / 1000);
				var secondsAll = seconds;
				if (seconds >= 0) {
					var minutes = Math.floor(seconds / 60);
					seconds -= minutes * 60;
					$("#minutes").text(minutes < 10 ? "0" + minutes : minutes);
					$("#seconds").text(seconds < 10 ? "0" + seconds : seconds);
					$("head title").text("["+secondsAll+"] "+initialTitle);
				} else {
					clearInterval(counter);
					waitComplete();
				}	
			}
		}

		updateTime();
		var counter = setInterval(updateTime, 500);		
	}
}