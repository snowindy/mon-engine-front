package com.github.snowindy.hibernate;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.cfg.ImprovedNamingStrategy;

public class UppercaseImprovedNamingStrategy extends ImprovedNamingStrategy {

    private static final long serialVersionUID = 1L;

    @Override
    public String tableName(String tableName) {
        return StringUtils.upperCase(super.tableName(tableName));
    }
    
    @Override
    public String classToTableName(String className) {
        return StringUtils.upperCase(super.classToTableName(className));
    }
}
