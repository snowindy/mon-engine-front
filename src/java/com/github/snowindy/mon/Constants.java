package com.github.snowindy.mon;

public class Constants {
    public static class Cache{
        public static final String REST = "rest";
        public static final String REST_SNAPSHOT = "rest-snapshot";
        public static final String REST_STATS = "rest-stats";
        public static final String PROXY_WAITS = "proxy-waits";
        public static final String PROXY_FREEPASS = "proxy-freepass";
    }
}
