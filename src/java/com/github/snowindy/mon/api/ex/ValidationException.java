package com.github.snowindy.mon.api.ex;

public class ValidationException extends Exception {
    private static final long serialVersionUID = 1L;
    
    public ValidationException(String cause) {
        super(cause);
    }
}
