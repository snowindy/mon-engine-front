package com.github.snowindy.util.text;

import java.util.ArrayList;
import java.util.List;

public class StringSplitUtils {
    public static List<String> splitByColon(String in) {
        if (in == null) {
            return new ArrayList<String>();
        }
        in = in.replaceAll("\\s", " ");
        in = in.replaceAll("\\s+", " ");
        String[] spl = in.split("[,;]");
        ArrayList<String> res = new ArrayList<String>();
        for (String w : spl) {
            w = w.trim();
            if (!w.isEmpty()) {
                res.add(w);
            }
        }
        return res;
    }

    public static boolean containsShortPhrase(String in, int minWordLength) {
        List<String> spl = splitByColon(in);
        return containsShortPhrase(spl, minWordLength);
    }

    public static boolean containsShortPhrase(List<String> inList, int minWordLength) {
        for (String word : inList) {
            if (word.length() <= minWordLength) {
                return true;
            }
        }
        return false;
    }
}
