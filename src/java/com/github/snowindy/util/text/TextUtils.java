package com.github.snowindy.util.text;

import java.awt.event.KeyEvent;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

public class TextUtils {

    public static String removeNonPrintableCharacters(String in) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < in.length(); i++) {
            Character c = in.charAt(i);
            if (isRegularChar(c)) {
                builder.append(c);
            }
        }
        return builder.toString();
    }

    private static Character tab = new Character('\t');
    private static Character nl = new Character('\n');

    /**
     * http://stackoverflow.com/questions/220547/printable-char-in-java
     */
    private static boolean isRegularChar(Character c) {
        Character.UnicodeBlock block = Character.UnicodeBlock.of(c);
        boolean res = (!Character.isISOControl(c)) && c != KeyEvent.CHAR_UNDEFINED && block != null
                && block != Character.UnicodeBlock.SPECIALS;
        if (!res) {
            res = tab.equals(c) || nl.equals(c);
        }

        return res;
    }

    public static boolean containsNonPrintableCharacters(String in) {
        for (int i = 0; i < in.length(); i++) {
            Character c = in.charAt(i);
            if (!isRegularChar(c)) {
                return true;
            }
        }
        return false;
    }

    private static final Pattern urlPattern = Pattern.compile("([^\\s#?]+)(\\?[^\\s#]*)?(#[^\\s]*)?", Pattern.MULTILINE
            | Pattern.CASE_INSENSITIVE);

    public static String addParametersToUrl(String url, List<? extends Pair<String, String>> params) {
        if (url == null) {
            throw new IllegalArgumentException("Null url argument");
        }
        if (params.size() == 0) {
            return url;
        }

        List<String> pairs = new ArrayList<String>();
        try {
            for (Pair<String, String> pair : params) {
                pairs.add(URLEncoder.encode(pair.getLeft(), "UTF-8") + "="
                        + URLEncoder.encode(StringUtils.defaultString(pair.getRight()), "UTF-8"));
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Should never be thrown.", e);
        }
        String appendParams = StringUtils.join(pairs, "&");

        Matcher m = urlPattern.matcher(url);
        boolean found = m.find();

        if (!found) {
            throw new IllegalArgumentException(String.format("Incorrect url input: '%s'.", url));
        }

        String base = m.group(1);
        String paramsStr = StringUtils.defaultString(m.group(2));
        String hashStr = StringUtils.defaultString(m.group(3));

        paramsStr = StringUtils.removeStart(paramsStr, "?");

        List<String> paramPairs = getParamPairs(paramsStr);
        removePairsWhichCollideWithNewParams(params, paramPairs);
        paramsStr = StringUtils.join(paramPairs, "&");
        paramsStr = "?" + paramsStr;

        if (!StringUtils.endsWith(paramsStr, "&") && !"?".equals(paramsStr)) {
            paramsStr = paramsStr + "&";
        }

        url = base + paramsStr + appendParams + hashStr;
        return url;
    }

    private static void removePairsWhichCollideWithNewParams(List<? extends Pair<String, String>> params,
                                                             List<String> paramPairs) {
        Iterator<String> iter = paramPairs.iterator();
        while (iter.hasNext()) {
            String paramPair = iter.next();
            for (Pair<String, String> pair : params) {
                if (paramPair.startsWith(pair.getKey())) {
                    iter.remove();
                    break;
                }
            }
        }
    }

    private static List<String> getParamPairs(String paramsStr) {
        List<String> paramPairs = new ArrayList<String>(Arrays.asList(StringUtils.split(paramsStr, "&")));
        for (int i = 0; i < paramPairs.size(); i++) {
            String pair = paramPairs.get(i);
            if (StringUtils.startsWith(pair, "amp;")) {
                paramPairs.set(i, StringUtils.removeStart(pair, "amp;"));
            }
        }
        return paramPairs;
    }

    public static String addParametersToUrl(String url, Map<String, String> params) {
        List<ImmutablePair<String, String>> list = new ArrayList<ImmutablePair<String, String>>();
        for (Map.Entry<String, String> e : params.entrySet()) {
            list.add(new ImmutablePair<String, String>(e.getKey(), e.getValue()));
        }
        return addParametersToUrl(url, list);
    }

}
