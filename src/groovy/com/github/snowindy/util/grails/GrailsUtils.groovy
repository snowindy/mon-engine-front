package com.github.snowindy.util.grails

import grails.util.Environment

import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.codehaus.groovy.grails.commons.DomainClassArtefactHandler
import org.codehaus.groovy.grails.commons.GrailsApplication
import org.codehaus.groovy.grails.web.binding.DataBindingUtils
import org.codehaus.groovy.grails.web.metaclass.BindDynamicMethod
import org.codehaus.groovy.grails.web.util.WebUtils

class GrailsUtils {
	
	static Logger log = Logger.getLogger(GrailsUtils.class)
	
	static def checkParentEntity(parentEntityParam, params, request, flash, controller){
		if (!params[parentEntityParam]){
			flash.message = controller.message(code: 'common.error.idNotPassed.message', args:[parentEntityParam, controller.getClass().simpleName, request.forwardURI, params])
			controller.redirect(controller:'root')
			true
		}
		false
	}
	
	//http://stackoverflow.com/questions/6338148/grails-find-domain-class-by-name
	static def getDomainClassBySimpleName(grailsApplication, simpleClassName){
		simpleClassName =  StringUtils.capitalize(simpleClassName)
		def obj = grailsApplication.domainClasses.find { it.clazz.simpleName == simpleClassName }
		obj?obj.clazz:null
	}
	
    static def isTestEnv(){
        Environment.current == Environment.TEST
    }
    
    static def getFlashScope(request){
        def grailsWebRequest = WebUtils.retrieveGrailsWebRequest()
        grailsWebRequest.attributes.getFlashScope(request)
    }
	
	static boolean isDomain(def dmnObj){
		boolean res = false
		try{
			res = DomainClassArtefactHandler.isDomainClass(dmnObj?.class)
		}catch(e){
		println "ROOOR ${e}"
			log.error(e)
		}
		res
	}
	
	//http://stackoverflow.com/questions/6301080/how-do-i-determine-if-a-class-is-a-grails-domain-object
	static def getControllerNameForDomain(def dmnObj){
		if (isDomain(dmnObj)){
			//replacing hibernate class addition _$$_javassist_123
			String simpleName = dmnObj.class.simpleName.replaceAll('_\\$\\$_javassist_\\d+', '')
			simpleName
		}
	}
	
	static GLOBAL_EXCLUDES = ['updateDate','createDate','metaClass']
		
	static def copyDomainFromMap(def domainTo, def domainFromProps, List excludes){
		List includesInitial = DataBindingUtils.getBindingIncludeList(domainTo)
		
		excludes = excludes + GLOBAL_EXCLUDES
		
		List includes = []
		includesInitial.each{
			if (!it.endsWith('*')){
				if (!excludes.contains(it)){
					includes << it
				}
			}
		}
		
		DataBindingUtils.bindObjectToInstance(domainTo, domainFromProps, includes, [], null)
	}
	
	static def copyDomain(def domainTo, def domainFrom, List excludes){
		copyDomainFromMap(domainTo, domainFrom.properties, excludes)
	}
    
    static boolean isConfigTrue(GrailsApplication grailsApplication, String configPath){
        grailsApplication.getConfig().flatten().get(configPath)
    }
    
    static <T> T bindData(T target, Map params){
        BindDynamicMethod bind = new BindDynamicMethod()
        def args =  [ target, params ]
        bind.invoke( target, 'bind', (Object[])args)
    }
    
    static void copyProperties(source, target, def includedFields = null) {
        source.properties.each { key, value ->
            if (target.hasProperty(key) && !(key in ['class', 'metaClass'])){
                if (!includedFields || key in includedFields){
                    target[key] = value
                }
            }
        }
    }
    
    static def propagateParamToModel(def model, def params, String paramName){
        if (params[paramName]){
            model[paramName] = params[paramName]
        }
    }
}
