package com.github.snowindy.mon.entity.enums

import java.util.List;

import groovy.transform.ToString

import org.apache.commons.lang3.StringUtils

import com.github.snowindy.util.text.StringSplitUtils

enum AggregationPolicy {
    FULL, MIXED, AVOIDED
}
